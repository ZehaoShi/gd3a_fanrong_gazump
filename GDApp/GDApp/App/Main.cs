#define DEMO

using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using JigLibX.Geometry;
using JigLibX.Collision;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Media;
/*
override Clone()
Reticule color not resetting
No statustype on controllers
mouse object text interleaving with progress controller
progress controller not receiving events to increase progress
Z-fighting on ground plane in 3rd person mode
Elevation angle on 3rd person view
ScreenManager - enum - this.ScreenType = (ScreenUtilityScreenType)eventData.AdditionalEventParameters[0];
check clone on new eventdata
PiP
menu - click sound
menu transparency
*/

namespace GDApp
{
    public class Main : Game
    {

        #region Fields
#if DEBUG
        //used to visualize debug info (e.g. FPS) and also to draw collision skins
        private DebugDrawer debugDrawer;
        private PhysicsDebugDrawer physicsDebugDrawer;
#endif

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //added property setters in short-hand form for speed
        public ObjectManager objectManager { get; private set; }
        public CameraManager cameraManager { get; private set; }
        public MouseManager mouseManager { get; private set; }
        public KeyboardManager keyboardManager { get; private set; }
        public ScreenManager screenManager { get; private set; }
        public MyAppMenuManager menuManager { get; private set; }
        public PhysicsManager physicsManager { get; private set; }
        public UIManager uiManager { get; private set; }
        public GamePadManager gamePadManager { get; private set; }
        public SoundManager soundManager { get; private set; }
        public PickingManager pickingManager { get; private set; }



        public MyHotKeyManager hotKeyManager { get; private set; }

        //receives, handles and routes events
        public EventDispatcher eventDispatcher { get; private set; }

        //stores loaded game resources
        private ContentDictionary<Model> modelDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private ContentDictionary<SpriteFont> fontDictionary;

        //stores curves and rails used by cameras, viewport, effect parameters
        private Dictionary<string, Transform3DCurve> curveDictionary;
        private Dictionary<string, RailParameters> railDictionary;
        private Dictionary<string, Viewport> viewPortDictionary;
        private Dictionary<string, EffectParameters> effectDictionary;
        private ContentDictionary<Video> videoDictionary;
        private Dictionary<string, IVertexData> vertexDataDictionary;


        private ManagerParameters managerParameters;

        //demo remove later
        private HeroPlayerObject heroPlayerObject;
        private ModelObject drivableBoxObject;
        private AnimatedPlayerObject animatedHeroPlayerObject;
        private PlayerCameraTarget testbox;
        public static int temp;
        public static int cloneCount;
        DudeAnimatedPlayerObject cloned;


        #endregion

        #region Properties
        #endregion

        #region Constructor
        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        #endregion

        #region Initialization
        protected override void Initialize()
        {
            //moved instanciation here to allow menu and ui managers to be moved to InitializeManagers()
            spriteBatch = new SpriteBatch(GraphicsDevice);

            int gameLevel = 1;
            bool isMouseVisible = true;
            Integer2 screenResolution = ScreenUtility.HD720;
            ScreenUtility.ScreenType screenType = ScreenUtility.ScreenType.SingleScreen;
            int numberOfGamePadPlayers = 1;

            //set the title
            Window.Title = "Gazump - Release 2";

            //EventDispatcher
            InitializeEventDispatcher();

            //Dictionaries, Media Assets and Non-media Assets
            LoadDictionaries();
            LoadAssets();
            LoadCurvesAndRails();
            LoadViewports(screenResolution);

            //to draw primitives and billboards
            LoadVertexData();

            //Effects
            InitializeEffects();

            //Managers
            InitializeManagers(screenResolution, screenType, isMouseVisible, numberOfGamePadPlayers);

#if DEBUG
            //InitializeDebugTextInfo();
#endif

            //load game happens before cameras are loaded because we may add a third person camera that needs a reference to a loaded Actor
            LoadGame(gameLevel);

            InitializeIntroCamera();

            InitializeBoxCamera(screenResolution);
            InitializeCollidableFirstPersonDemo(screenResolution);
            InitializePlayer();

            //Initialize cameras based on the desired screen layout


            //menu and UI elements
            AddMenuElements();
            AddUIElements();

            // initialize vehicle models
            InitializeVehicles();

            //initial architecture models
            InitializeBuildings();

            //Publish Start Event(s)
            StartGame();

#if DEBUG
            //InitializeDebugCollisionSkinInfo();
#endif

            base.Initialize();
        }

        private void InitializeBuildings()
        {
            //Model download from https://sketchfab.com/features/download and https://www.turbosquid.com/Search/3D-Models/free
            
            Model model = null;
            Transform3D transform = new Transform3D(new Vector3(0, 0, 0), new Vector3(1, 1, 1));
            ModelObject modelObject = new ModelObject("building", ActorType.Player, transform, this.effectDictionary[AppData.LitModelsEffectID].Clone() as EffectParameters, model);
            //this.objectManager.Add(modelObject);

            //bus stop
            ModelObject clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["bus_stop"];
            clone.EffectParameters.Texture = this.textureDictionary["hotelTexture"];
            clone.InitializeBoneTransforms();
            clone.Transform.TranslateTo(new Vector3(200,0,-65));
            clone.Transform.RotateAroundYBy(95);
            clone.Transform.Scale = new Vector3(0.15f, 0.15f, 0.15f);
            this.objectManager.Add(clone);

            clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["bus_stop"];
            clone.EffectParameters.Texture = this.textureDictionary["hotelTexture"];
            clone.InitializeBoneTransforms();
            clone.Transform.TranslateTo(new Vector3(-400, 0, -620));
            clone.Transform.RotateAroundYBy(95);
            clone.Transform.Scale = new Vector3(0.15f, 0.15f, 0.15f);
            this.objectManager.Add(clone);

            clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["bus_stop"];
            clone.EffectParameters.Texture = this.textureDictionary["hotelTexture"];
            clone.InitializeBoneTransforms();
            clone.Transform.TranslateTo(new Vector3(280, 0, -450));
            clone.Transform.RotateAroundYBy(35);
            clone.Transform.Scale = new Vector3(0.15f, 0.15f, 0.15f);
            this.objectManager.Add(clone);


            //GYM
            clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["gym"];
            clone.InitializeBoneTransforms();
            clone.EffectParameters.Texture = this.textureDictionary["gymTexture"];
            clone.Transform.TranslateBy(new Vector3(-800, 0, -480));
            clone.Transform.RotateAroundYBy(90);
            clone.Transform.Scale = new Vector3(0.8f, 0.8f, 0.8f);
            this.objectManager.Add(clone);

            clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["gym"];
            clone.InitializeBoneTransforms();
            clone.EffectParameters.Texture = this.textureDictionary["gymTexture"];
            clone.Transform.TranslateBy(new Vector3(450, 0, -850));
            clone.Transform.RotateAroundYBy(0);
            clone.Transform.Scale = new Vector3(0.8f, 0.8f, 0.8f);
            this.objectManager.Add(clone);

            //Apartment
            clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["Apartment"];
            clone.InitializeBoneTransforms();
            clone.EffectParameters.Texture = this.textureDictionary["Apartment"];
            clone.Transform.Scale = new Vector3(0.15f, 0.15f, 0.15f);
            clone.Transform.TranslateBy(new Vector3(-800, 0, -280));
            this.objectManager.Add(clone);

            clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["Apartment2"];
            clone.InitializeBoneTransforms();
            clone.EffectParameters.Texture = this.textureDictionary["Apartment2"];
            clone.Transform.Scale = new Vector3(0.15f, 0.15f, 0.15f);
            clone.Transform.RotateAroundYBy(-90);
            clone.Transform.TranslateBy(new Vector3(-450, 0, -800));
            this.objectManager.Add(clone);

            clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["Apartment"];
            clone.InitializeBoneTransforms();
            clone.EffectParameters.Texture = this.textureDictionary["Apartment"];
            clone.Transform.Scale = new Vector3(0.15f, 0.15f, 0.15f);
            clone.Transform.RotateAroundYBy(-90);
            clone.Transform.TranslateBy(new Vector3(-250, 0, -800));
            this.objectManager.Add(clone);

            clone = modelObject.Clone() as ModelObject;
            clone.Model = this.modelDictionary["Apartment2"];
            clone.InitializeBoneTransforms();
            clone.EffectParameters.Texture = this.textureDictionary["Apartment2"];
            clone.Transform.Scale = new Vector3(0.15f, 0.15f, 0.15f);
            clone.Transform.RotateAroundYBy(-90);
            clone.Transform.TranslateBy(new Vector3(150, 0, -800));
            this.objectManager.Add(clone);



        }

        private void InitializeVehicles()
        {

            Vector3[] route = null;
            defineVehicleRoutes(ref route);

            //bus
            Model model = this.modelDictionary["Bus"];
            Transform3D transform = new Transform3D(new Vector3(0, 0, 0), new Vector3(0.05f, 0.05f, 0.05f));
            EffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as EffectParameters;

            effectParameters.Texture = this.textureDictionary["Car_12"];

            ModelObject modelObject = new ModelObject("vehicle", ActorType.Player, transform, effectParameters, model);
            modelObject.Transform.Translation = route[14];

            modelObject.AttachController(new RouteControllerWithLook("Car Controller", ControllerType.Track, route, 50, 14));
            this.objectManager.Add(modelObject);

            ModelObject clone = modelObject.Clone() as ModelObject;

            clone.Model = this.modelDictionary["Bus"];
            clone.EffectParameters.Texture = this.textureDictionary["Car_11"];
            clone.Transform.Translation = route[3];

            clone.AttachController(new RouteControllerWithLook("Car Controller", ControllerType.Track, route, 50, 3));
            this.objectManager.Add(clone);

            //truck
            for (int i = 0; i < 6; i++)
            {
                Transform3D transform2 = new Transform3D(new Vector3(110 + 30 * i, 3, -20 - 3 * i), new Vector3(5, 180, -90), new Vector3(0.05f, 0.05f, 0.05f), Vector3.UnitX, Vector3.UnitY);
                clone = modelObject.Clone() as ModelObject;
                clone.Transform = transform2;
                clone.Model = this.modelDictionary["Truck"];
                clone.EffectParameters.Texture = this.textureDictionary["Car_04"];
                this.objectManager.Add(clone);
            }

            for (int i = 0; i < 3; i++)
            {
                Transform3D transform2 = new Transform3D(new Vector3(400 + 30 * i, 3, -50 - 3 * i), new Vector3(5, 180, -90), new Vector3(0.05f, 0.05f, 0.05f), Vector3.UnitX, Vector3.UnitY);
                clone = modelObject.Clone() as ModelObject;
                clone.Transform = transform2;
                clone.Model = this.modelDictionary["Truck"];
                clone.EffectParameters.Texture = this.textureDictionary["Car_04"];
                this.objectManager.Add(clone);
            }


            //car

            clone = modelObject.Clone() as ModelObject;

            clone.Model = this.modelDictionary["Car"];
            clone.EffectParameters.Texture = this.textureDictionary["Car_07"];
            clone.Transform.Translation = route[8];

            clone.AttachController(new RouteControllerWithLook("Car Controller", ControllerType.Track, route, 50, 8));
            this.objectManager.Add(clone);

            clone = modelObject.Clone() as ModelObject;

            clone.Model = this.modelDictionary["Car"];
            clone.EffectParameters.Texture = this.textureDictionary["Car_08"];
            clone.Transform.Translation = route[0];

            clone.AttachController(new RouteControllerWithLook("Car Controller", ControllerType.Track, route, 50, 0));
            this.objectManager.Add(clone);


        }

        private void defineVehicleRoutes(ref Vector3[] route)
        {
            route = new Vector3[] { new Vector3(557.0768f,  0, -238.2944f),
                                    new Vector3(505.9135f,  0, -167.2639f),
                                    new Vector3(381.0493f,  0, -124.5942f),
                                    new Vector3(-337.5945f, 0, -58.20867f),
                                    new Vector3(-563.0579f, 0, -48.0498f),
                                    new Vector3(-592.2563f, 0, -57.42733f),
                                    new Vector3(-634.2847f, 0, -104.5472f),
                                    new Vector3(-648.7372f, 0, -143.8206f),
                                    new Vector3(-647.5066f, 0, -605.095f),
                                    new Vector3(-632.9497f, 0, -647.1549f),
                                    new Vector3(-590.6917f, 0, -684.4395f),
                                    new Vector3(-552.6288f, 0, -693.1831f),
                                    new Vector3(234.5336f,  0, -692.6871f),
                                    new Vector3(267.3492f,  0, -682.27f),
                                    new Vector3(309.2626f,  0, -627.6385f),
                                    new Vector3(315.0088f,  0, -558.7705f),
                                    new Vector3(373.4648f,  0, -462.4668f),
                                    new Vector3(506.2513f,  0, -360.6898f),
                                    new Vector3(540.342f, 0, -325.1838f),
                                    new Vector3(560.6974f, 0, -287.7069f)
                                  };
        }

        private void InitializeManagers(Integer2 screenResolution,
            ScreenUtility.ScreenType screenType, bool isMouseVisible, int numberOfGamePadPlayers) //1 - 4
        {
            //add sound manager
            this.soundManager = new SoundManager(this, this.eventDispatcher, StatusType.Update, "Content/Assets/Audio/", "Demo2DSound.xgs", "WaveBank1.xwb", "SoundBank1.xsb");
            Components.Add(this.soundManager);

            this.cameraManager = new CameraManager(this, 1, this.eventDispatcher);
            Components.Add(this.cameraManager);

            //create the object manager - notice that its not a drawablegamecomponent. See ScreeManager::Draw()
            this.objectManager = new ObjectManager(this, this.cameraManager, this.eventDispatcher, 10);

            //add keyboard manager
            this.keyboardManager = new KeyboardManager(this);
            Components.Add(this.keyboardManager);

            //create the manager which supports multiple camera viewports
            this.screenManager = new ScreenManager(this, graphics, screenResolution, screenType,
                this.objectManager, this.cameraManager, this.keyboardManager,
                AppData.KeyPauseShowMenu, this.eventDispatcher, StatusType.Off);
            Components.Add(this.screenManager);

            //CD-CR using JigLibX and add debug drawer to visualise collision skins
            this.physicsManager = new PhysicsManager(this, this.eventDispatcher, StatusType.Off, AppData.BigGravity);
            Components.Add(this.physicsManager);

            //add mouse manager
            this.mouseManager = new MouseManager(this, isMouseVisible, this.physicsManager);
            Components.Add(this.mouseManager);

            //add gamepad manager
            if (numberOfGamePadPlayers > 0)
            {
                this.gamePadManager = new GamePadManager(this, numberOfGamePadPlayers);
                Components.Add(this.gamePadManager);
            }

            //menu manager
            this.menuManager = new MyAppMenuManager(this, this.mouseManager, this.keyboardManager, this.cameraManager, spriteBatch, this.eventDispatcher, StatusType.Off);
            //set the main menu to be the active menu scene
            this.menuManager.SetActiveList("start_menu");
            Components.Add(this.menuManager);

            this.hotKeyManager = new MyHotKeyManager(this, this.mouseManager, this.keyboardManager, this.cameraManager, spriteBatch, this.eventDispatcher, StatusType.Off, this.player);
            this.hotKeyManager.SetActiveList("inGame");
            Components.Add(this.hotKeyManager);

            //ui (e.g. reticule, inventory, progress)
            this.uiManager = new UIManager(this, this.spriteBatch, this.eventDispatcher, 10, StatusType.Off);
            Components.Add(this.uiManager);


            //this object packages together all managers to give the mouse object the ability to listen for all forms of input from the user, as well as know where camera is etc.
            this.managerParameters = new ManagerParameters(this.objectManager,
                this.cameraManager, this.mouseManager, this.keyboardManager, this.gamePadManager, this.screenManager, this.soundManager);

            #region Pick Manager
            //call this function anytime we want to decide if a mouse over object is interesting to the PickingManager
            //See https://www.codeproject.com/Articles/114931/Understanding-Predicate-Delegates-in-C
            Predicate<CollidableObject> collisionPredicate = new Predicate<CollidableObject>(CollisionUtility.IsCollidableObjectOfInterest);
            //create the projectile archetype that the manager can fire

            //listens for picking with the mouse on valid (based on specified predicate) collidable objects and pushes notification events to listeners
            this.pickingManager = new PickingManager(this, this.eventDispatcher, StatusType.Off,
                this.managerParameters,
                PickingBehaviourType.PickAndPlace, AppData.PickStartDistance, AppData.PickEndDistance, collisionPredicate);
            Components.Add(this.pickingManager);
            #endregion
        }

        private void LoadDictionaries()
        {
            //models
            this.modelDictionary = new ContentDictionary<Model>("model dictionary", this.Content);

            //textures
            this.textureDictionary = new ContentDictionary<Texture2D>("texture dictionary", this.Content);

            //fonts
            this.fontDictionary = new ContentDictionary<SpriteFont>("font dictionary", this.Content);

            //curves - notice we use a basic Dictionary and not a ContentDictionary since curves and rails are NOT media content
            this.curveDictionary = new Dictionary<string, Transform3DCurve>();

            //rails
            this.railDictionary = new Dictionary<string, RailParameters>();

            //viewports - used to store different viewports to be applied to multi-screen layouts
            this.viewPortDictionary = new Dictionary<string, Viewport>();

            //stores default effect parameters
            this.effectDictionary = new Dictionary<string, EffectParameters>();

            //notice we go back to using a content dictionary type since we want to pass strings and have dictionary load content
            this.videoDictionary = new ContentDictionary<Video>("video dictionary", this.Content);

            //used to store IVertexData (i.e. when we want to draw primitive objects, as in I-CA)
            this.vertexDataDictionary = new Dictionary<string, IVertexData>();

        }

        private void LoadAssets()
        {
            #region Models
            //geometric samples
            this.modelDictionary.Load("Assets/Models/plane1", "plane1");
            this.modelDictionary.Load("Assets/Models/plane", "plane");
            this.modelDictionary.Load("Assets/Models/box2", "box2");

            this.modelDictionary.Load("Assets/Models/torus");
            this.modelDictionary.Load("Assets/Models/sphere");

            //triangle mesh high/low poly demo
            this.modelDictionary.Load("Assets/Models/teapot");
            this.modelDictionary.Load("Assets/Models/teapot_mediumpoly");
            this.modelDictionary.Load("Assets/Models/teapot_lowpoly");

            //player - replace with animation eventually
            this.modelDictionary.Load("Assets/Models/cylinder");

            //architecture
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/house");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/shoppingCenter");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/hotel");
            this.modelDictionary.Load("Assets/Models/Architecture/Walls/wall");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/roadCollision");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/room");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/gym");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/bus_stop");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/Apartment");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/Apartment2");
            this.modelDictionary.Load("Assets/Models/Architecture/Buildings/Residence");

            this.modelDictionary.Load("Assets/Models/Tree");
            this.modelDictionary.Load("Assets/Models/tree2");

            //dual texture demo
            this.modelDictionary.Load("Assets/Models/box");
            this.modelDictionary.Load("Assets/Models/box1");

            //vehicles
            this.modelDictionary.Load("Assets/Models/Props/Bus");
            this.modelDictionary.Load("Assets/Models/Props/Truck");
            this.modelDictionary.Load("Assets/Models/Props/Car");
            #endregion

            #region Textures
            //environment
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate1"); //demo use of the shorter form of Load() that generates key from asset name
            this.textureDictionary.Load("Assets/Textures/Props/Crates/crate2");
            this.textureDictionary.Load("Assets/GDDebug/Textures/checkerboard");
            this.textureDictionary.Load("Assets/Textures/Foliage/Ground/newGround");
            this.textureDictionary.Load("Assets/Textures/Skybox/back");
            this.textureDictionary.Load("Assets/Textures/Skybox/left");
            this.textureDictionary.Load("Assets/Textures/Skybox/right");
            this.textureDictionary.Load("Assets/Textures/Skybox/sky");
            this.textureDictionary.Load("Assets/Textures/Skybox/front");
            this.textureDictionary.Load("Assets/Textures/Foliage/Trees/treeTexture");
            this.textureDictionary.Load("Assets/Textures/Foliage/Trees/tree2texture");

            //this.textureDictionary.Load("Assets/Textures/Foliage/Trees/bark_0021");
            //this.textureDictionary.Load("Assets/Textures/Foliage/Trees/DB2X2_L01");
            //this.textureDictionary.Load("Assets/Textures/Foliage/Trees/DB2X2_L01_Spec");

            //dual texture demo
            this.textureDictionary.Load("Assets/Textures/Foliage/Ground/grass_midlevel");
            this.textureDictionary.Load("Assets/Textures/Foliage/Ground/grass_highlevel");

            //menu - buttons
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/blankButton");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/startButton");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/optionsButton");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/exitButton");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/helpBtn");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/ruleButton");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Buttons/controlBtn");



            //menu - backgrounds
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/start_menu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/audiomenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/controlsmenu");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/exitmenuwithtrans");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/helpPageBG");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/control");
            this.textureDictionary.Load("Assets/Textures/UI/Menu/Backgrounds/rule");

            //ui (or hud) elements
            this.textureDictionary.Load("Assets/Textures/UI/HUD/reticuleDefault");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/progress_gradient");

            this.textureDictionary.Load("Assets/Textures/UI/HUD/gameOver");



            this.textureDictionary.Load("Assets/Textures/UI/HUD/HUDTop");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/incomeBox");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/houseButton");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/centreButton");
            this.textureDictionary.Load("Assets/Textures/UI/HUD/hotelButton");


            //architecture
            //this.textureDictionary.Load("Assets/Textures/Architecture/Buildings/house-low-texture");
            this.textureDictionary.Load("Assets/Textures/Architecture/Buildings/house-Texture");
            this.textureDictionary.Load("Assets/Textures/Architecture/Buildings/hotelTexture");
            this.textureDictionary.Load("Assets/Textures/Architecture/Buildings/RealHotelTexture");
            this.textureDictionary.Load("Assets/Textures/Architecture/Buildings/gymTexture");
            this.textureDictionary.Load("Assets/Textures/Architecture/Buildings/Apartment");
            this.textureDictionary.Load("Assets/Textures/Architecture/Buildings/Apartment2");
            
            
   
            this.textureDictionary.Load("Assets/Textures/Architecture/Walls/wall");

            //dual texture demo - see Main::InitializeCollidableGround()
            this.textureDictionary.Load("Assets/GDDebug/Textures/checkerboard_greywhite");

            //vehicles
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_12");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_11");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_10");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_09");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_08");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_07");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_06");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_05");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_04");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_03");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_02");
            this.textureDictionary.Load("Assets/Textures/Props/Vehicles/Car_01");

#if DEBUG
            //demo
            this.textureDictionary.Load("Assets/GDDebug/Textures/ml");
            this.textureDictionary.Load("Assets/GDDebug/Textures/checkerboard");
#endif
            #endregion

            #region Fonts
#if DEBUG
            this.fontDictionary.Load("Assets/GDDebug/Fonts/debug");
#endif
            this.fontDictionary.Load("Assets/Fonts/menu");
            this.fontDictionary.Load("Assets/Fonts/mouse");
            this.fontDictionary.Load("Assets/Fonts/inGame");
            this.fontDictionary.Load("Assets/Fonts/gameOver");
            #endregion

            #region Video
            this.videoDictionary.Load("Assets/Video/sample");
            #endregion

            #region Animations
            //contains a single animation "Take001"
            this.modelDictionary.Load("Assets/Models/Animated/dude");
            this.modelDictionary.Load("Assets/Models/Animated/student");
            this.modelDictionary.Load("Assets/Models/Animated/Running");

            //squirrel - one file per animation
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Idle");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Jump");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Punch");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Standing");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/Red_Tailwhip");
            this.modelDictionary.Load("Assets/Models/Animated/Squirrel/RedRun4");
            #endregion

        }

        private void LoadCurvesAndRails()
        {
            int cameraHeight = 5;

            #region Curves
            //create the camera curve to be applied to the track controller
            Transform3DCurve curveA = new Transform3DCurve(CurveLoopType.Oscillate); //experiment with other CurveLoopTypes
            curveA.Add(new Vector3(40, cameraHeight, 80), -Vector3.UnitX, Vector3.UnitY, 0); //start position
            curveA.Add(new Vector3(0, 10, 60), -Vector3.UnitZ, Vector3.UnitY, 4);
            curveA.Add(new Vector3(0, 40, 0), -Vector3.UnitY, -Vector3.UnitZ, 8); //curve mid-point
            curveA.Add(new Vector3(0, 10, 60), -Vector3.UnitZ, Vector3.UnitY, 12);
            curveA.Add(new Vector3(40, cameraHeight, 80), -Vector3.UnitX, Vector3.UnitY, 16); //end position - same as start for zero-discontinuity on cycle
            //add to the dictionary
            this.curveDictionary.Add("unique curve name 1", curveA);
            #endregion

            #region Rails
            //create the track to be applied to the non-collidable track camera 1
            this.railDictionary.Add("rail1 - parallel to x-axis", new RailParameters("rail1 - parallel to x-axis", new Vector3(-80, 10, 40), new Vector3(80, 10, 40)));
            #endregion

        }

        private void LoadVertexData()
        {
            Microsoft.Xna.Framework.Graphics.PrimitiveType primitiveType;
            int primitiveCount;
            IVertexData vertexData = null;

            #region Textured Quad
            //get vertices for textured quad
            VertexPositionColorTexture[] vertices = VertexFactory.GetTextureQuadVertices(out primitiveType, out primitiveCount);

            //make a vertex data object to store and draw the vertices
            vertexData = new BufferedVertexData<VertexPositionColorTexture>(this.graphics.GraphicsDevice, vertices, primitiveType, primitiveCount);

            //add to the dictionary for use by things like billboards - see InitializeBillboards()
            this.vertexDataDictionary.Add(AppData.TexturedQuadID, vertexData);
            #endregion

            #region Billboard Quad - we must use this type when creating billboards
            // get vertices for textured billboard
            VertexBillboard[] verticesBillboard = VertexFactory.GetVertexBillboard(1, out primitiveType, out primitiveCount);

            //make a vertex data object to store and draw the vertices
            vertexData = new BufferedVertexData<VertexBillboard>(this.graphics.GraphicsDevice, verticesBillboard, primitiveType, primitiveCount);

            //add to the dictionary for use by things like billboards - see InitializeBillboards()
            this.vertexDataDictionary.Add(AppData.TexturedBillboardQuadID, vertexData);
            #endregion

        }

        private void LoadViewports(Integer2 screenResolution)
        {

            //the full screen viewport with optional padding
            int leftPadding = 0, topPadding = 0, rightPadding = 0, bottomPadding = 0;
            Viewport paddedFullViewPort = ScreenUtility.Pad(new Viewport(0, 0, screenResolution.X, (int)(screenResolution.Y)), leftPadding, topPadding, rightPadding, bottomPadding);
            this.viewPortDictionary.Add("full viewport", paddedFullViewPort);

            //work out the dimensions of the small camera views along the left hand side of the screen
            int smallViewPortHeight = 144; //6 small cameras along the left hand side of the main camera view i.e. total height / 5 = 720 / 5 = 144
            int smallViewPortWidth = 5 * smallViewPortHeight / 3; //we should try to maintain same ProjectionParameters aspect ratio for small cameras as the large     
            //the five side viewports in multi-screen mode
            this.viewPortDictionary.Add("column0 row0", new Viewport(0, 0, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row1", new Viewport(0, 1 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row2", new Viewport(0, 2 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row3", new Viewport(0, 3 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            this.viewPortDictionary.Add("column0 row4", new Viewport(0, 4 * smallViewPortHeight, smallViewPortWidth, smallViewPortHeight));
            //the larger view to the right in column 1
            this.viewPortDictionary.Add("column1 row0", new Viewport(smallViewPortWidth, 0, screenResolution.X - smallViewPortWidth, screenResolution.Y));

            //picture-in-picture viewport
            Integer2 viewPortDimensions = new Integer2(240, 150); //set to 16:10 ratio as with screen dimensions
            int verticalOffset = 20;
            int rightHorizontalOffset = 20;
            this.viewPortDictionary.Add("PIP viewport", new Viewport((screenResolution.X - viewPortDimensions.X - rightHorizontalOffset),
                verticalOffset, viewPortDimensions.X, viewPortDimensions.Y));
        }

#if DEBUG
        private void InitializeDebugTextInfo()
        {
            //add debug info in top left hand corner of the screen
            this.debugDrawer = new DebugDrawer(this, this.managerParameters, spriteBatch,
                this.fontDictionary["debug"], Color.Black, new Vector2(5, 5), this.eventDispatcher, StatusType.Off);
            Components.Add(this.debugDrawer);

        }

        private void InitializeDebugCollisionSkinInfo()
        {
            //show the collision skins
            this.physicsDebugDrawer = new PhysicsDebugDrawer(this, this.cameraManager, this.objectManager,
                this.screenManager, this.eventDispatcher, StatusType.Off);
            Components.Add(this.physicsDebugDrawer);
        }
#endif
        #endregion

        #region Load Game Content
        //load the contents for the level specified
        private void LoadGame(int level)
        {

            int worldScale = 1300;

            InitializeNonCollidableSkyBox(worldScale);
            InitializeCollidableGround(worldScale);

            InitializePlayerTargetAndCursor();
            //add animated characters
            //bool bTheDudeAbides = true;
            //if (bTheDudeAbides)
            InitializeDudeAnimatedPlayer();
            //else
            //InitializeSquirrelAnimatedPlayer();


            InitializeTrees();
            InitializeNPCs();

            InitializeAmbientSounds();


        }

        private void InitializeAmbientSounds()  
        {
            // background Music
            Sound2D bgm = new Sound2D("BackgroundMusicLoop", this.soundManager, this.eventDispatcher);

            Transform3D transform = new Transform3D(new Vector3(0,0,-350), Vector3.One);
            Sound3D ambience = new Sound3D("ambience", ActorType.Sound3D, transform.Clone() as Transform3D, StatusType.Off, this.soundManager, "children_playing_loop", this.eventDispatcher);
            this.soundManager.playCue3D(ambience.sound);
            this.soundManager.PauseCue3D(ambience.sound);

            this.objectManager.Add(ambience);

            transform.Translation = new Vector3(500, 0, -350);
            ambience = new Sound3D("ambience", ActorType.Sound3D, transform.Clone() as Transform3D, StatusType.Off, this.soundManager, "bird_loop", this.eventDispatcher);
            this.soundManager.playCue3D(ambience.sound);
            this.soundManager.PauseCue3D(ambience.sound);

            this.objectManager.Add(ambience);

            transform.Translation = new Vector3(-350, 0, 200);
            ambience = new Sound3D("ambience", ActorType.Sound3D, transform.Clone() as Transform3D, StatusType.Off, this.soundManager, "car_alarm_loop", this.eventDispatcher);
            this.soundManager.playCue3D(ambience.sound);
            this.soundManager.PauseCue3D(ambience.sound);

            this.objectManager.Add(ambience);

            // moving sounds!!!
            Vector3[] vehiclePath = null;

            defineVehicleRoutes(ref vehiclePath);

            transform.Translation = vehiclePath[0];
            ambience = new Sound3D("ambience", ActorType.Sound3D, transform.Clone() as Transform3D, StatusType.Off, this.soundManager, "car_passing_loop", this.eventDispatcher);
            this.soundManager.playCue3D(ambience.sound);
            this.soundManager.PauseCue3D(ambience.sound);

            ambience.AttachController(new RouteController("vehicle sound", ControllerType.Sound, vehiclePath, 20, 0));

            this.objectManager.Add(ambience);

            transform.Translation = vehiclePath[8];
            ambience = new Sound3D("ambience", ActorType.Sound3D, transform.Clone() as Transform3D, StatusType.Off, this.soundManager, "car_horns_loop", this.eventDispatcher);
            this.soundManager.playCue3D(ambience.sound);
            this.soundManager.PauseCue3D(ambience.sound);

            ambience.AttachController(new RouteController("vehicle sound", ControllerType.Sound, vehiclePath, 30, 8));

            transform.Translation = vehiclePath[14];
            ambience = new Sound3D("ambience", ActorType.Sound3D, transform.Clone() as Transform3D, StatusType.Off, this.soundManager, "bus_engine_loop", this.eventDispatcher);
            this.soundManager.playCue3D(ambience.sound);
            this.soundManager.PauseCue3D(ambience.sound);

            ambience.AttachController(new RouteController("vehicle sound", ControllerType.Sound, vehiclePath, 10, 14));

            this.objectManager.Add(ambience);
        }

        private void InitializeNPCs()
        {
            Transform3D transform = new Transform3D(new Vector3(100, 0, 200), new Vector3(0.055f, 0.055f, 0.055f));
            transform.Look = new Vector3(0, 0, 1);
            AnimatedModelObject citizen = new AnimatedModelObject("citizen", ActorType.NPC, transform, this.effectDictionary[AppData.LitModelsEffectID].Clone() as EffectParameters, this.modelDictionary["student"], "Take 001");
            //transform 3D
            Vector3[] points = null;

            initNPCCurve(ref points);

            // clone randomly
            populateCityWithNPC(ref citizen, ref points);
        }

        private void populateCityWithNPC(ref AnimatedModelObject citizen, ref Vector3[] points)
        {
            int maxNPC = 15;
            HashSet<int> time = new HashSet<int>();

            for (int i = 0; i < maxNPC; i++) {

                Random rand = new Random();

                while (true)
                {
                    int generated = rand.Next(0, points.Length-1);

                    if (!time.Contains(generated))
                    {
                        AnimatedModelObject clonedNPC = citizen.Clone() as AnimatedModelObject;
                        //clonedNPC.Transform.Look = Vector3.UnitZ;
                        clonedNPC.Transform.Translation = points[generated];
                        //controller
                        clonedNPC.AttachController(new RouteControllerWithLook("NPC Route Controller", ControllerType.Track, points, 8, generated));

                        this.objectManager.Add(clonedNPC);

                        time.Add(generated);

                        break;
                    }
                }
           }
        }

        private void initNPCCurve(ref Vector3[] points)
        {
            points = new Vector3[]{new Vector3( 291.8859f, 0, -68.98111f) ,
                                new Vector3( -331.1064f, 0, -8.9729f)  ,
                                new Vector3( -549.8259f, 0, -2.681578f),
                                new Vector3( -579.425f, 0, 4.118966f) ,
                                new Vector3( -579.8497f, 0, -23.35798f),
                                new Vector3( -576.7742f, 0, -577.4435f),
                                new Vector3( -575.6579f, 0, -619.3892f),
                                new Vector3( -542.6776f, 0, -621.0617f),
                                new Vector3( 204.5387f, 0, -620.0067f) ,
                                new Vector3( 237.6367f, 0, -620.6226f) ,
                                new Vector3( 239.8984f, 0, -586.7698f) ,
                                new Vector3( 240.0051f, 0, -530.565f) ,
                                new Vector3( 306.8437f, 0, -413.6301f) ,
                                new Vector3( 570.4142f, 0, -200.72f)   ,
                                new Vector3( 591.5657f, 0, -181.5626f) ,
                                new Vector3( 573.6213f, 0, -166.8975f) ,
                                new Vector3( 530.103f, 0, -120.5829f)  ,
                                new Vector3( 395.7977f, 0, -77.00237f) ,
                                new Vector3( 291.8859f, 0, -68.98111f)}; // 19
        }

        private float calculateNextTime(Vector3 vector1, Vector3 vector2, ref float startTime)
        {
            float speed = 5.25f;
            float distance = (vector1 - vector2).Length();
            startTime += distance / speed;

            return startTime;
        }

        private void InitializePlayer()
        {
            this.player = new Player(this,
                                "Depending on Input",
                                AppData.startBalance, AppData.startPopulation,
                                AppData.startCapacity, this.keyboardManager,
                                this.mouseManager, this.objectManager,
                                this.effectDictionary[AppData.LitModelsEffectID],
                                this.modelDictionary, this.textureDictionary,
                                this.cameraManager.cameraList.Find((camera) => (camera.ID == "player camera")), this.eventDispatcher);
            Components.Add(player);
            this.hotKeyManager.setPlayer(player);
        }

        private void InitializePrimitives()
        {
            //get a copy of the effect parameters
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnLitPrimitivesEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            effectParameters.DiffuseColor = Color.Yellow;
            effectParameters.Alpha = 0.4f;

            //define location
            Transform3D transform = new Transform3D(new Vector3(0, 40, 0), new Vector3(40, 4, 1));

            //create primitive
            PrimitiveObject primitiveObject = new PrimitiveObject("simple primitive", ActorType.Primitive,
                transform, effectParameters, StatusType.Drawn | StatusType.Update, this.vertexDataDictionary[AppData.TexturedQuadID]);

            PrimitiveObject clonedPrimitiveObject = null;

            for (int i = 1; i <= 4; i++)
            {
                clonedPrimitiveObject = primitiveObject.Clone() as PrimitiveObject;
                clonedPrimitiveObject.Transform.Translation += new Vector3(0, 5 * i, 0);

                //we could also attach controllers here instead to give each a different rotation
                clonedPrimitiveObject.AttachController(new RotationController("rot controller", ControllerType.Rotation, new Vector3(0.1f * i, 0, 0)));

                //add to manager
                this.objectManager.Add(clonedPrimitiveObject);
            }

        }

        private void InitializeSquirrelAnimatedPlayer()
        {
            Transform3D transform3D = null;

            transform3D = new Transform3D(new Vector3(0, 20, 30),
                new Vector3(-90, 0, 0), //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                 2f * Vector3.One,
                 Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.DiffuseColor = Color.OrangeRed;
            //no specular, emissive, directional lights
            //if we dont specify a texture then the object manager will draw using whatever textures were baked into the animation in 3DS Max
            effectParameters.Texture = this.textureDictionary["checkerboard_greywhite"];

            this.animatedHeroPlayerObject = new SquirrelAnimatedPlayerObject("squirrel",
            ActorType.Player, transform3D,
                effectParameters,
                AppData.PlayerOneMoveKeys,
                AppData.PlayerRadius,
                AppData.PlayerHeight,
                1, 1,  //accel, decel
                AppData.SquirrelPlayerMoveSpeed,
                AppData.SquirrelPlayerRotationSpeed,
                AppData.PlayerJumpHeight,
                new Vector3(0, 0, 0), //offset inside capsule
                this.keyboardManager);
            this.animatedHeroPlayerObject.Enable(false, AppData.PlayerMass);

            //add the animations
            string takeName = "Take 001";
            string fileNameNoSuffix = "Red_Idle";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Jump";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Punch";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Standing";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "Red_Tailwhip";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);
            fileNameNoSuffix = "RedRun4";
            this.animatedHeroPlayerObject.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);

            //set the start animtion
            this.animatedHeroPlayerObject.SetAnimation("Take 001", "RedRun4");

            this.objectManager.Add(animatedHeroPlayerObject);

        }

        private void InitializeDudeAnimatedPlayer()
        {
            Random ran = new Random();

            Transform3D transform3D = null;

            transform3D = new Transform3D(new Vector3(0, 10, 40),
                new Vector3(-90, 0, 0), //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                 0.1f * Vector3.One,
                 -Vector3.UnitZ, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            //remember we can set diffuse color and alpha too but not specular, emissive, directional lights as I dont read those parameters in ObjectManager::DrawObject() - this was purely a time constraint on my part.
            effectParameters.DiffuseColor = Color.White;
            effectParameters.Alpha = 1;
            //if we dont specify a texture then the object manager will draw using whatever textures were baked into the animation in 3DS Max
            effectParameters.Texture = null;

            DudeAnimatedPlayerObject animatedHeroPlayerObject = new DudeAnimatedPlayerObject("Running",
                    ActorType.Player, transform3D,
                effectParameters,
                AppData.SquirrelPlayerMoveKeys,
                AppData.PlayerRadius,
                AppData.PlayerHeight,
                1, 1,  //accel, decel
                AppData.DudeMoveSpeed,
                AppData.DudeRotationSpeed,
                AppData.DudeJumpHeight,
                new Vector3(0, -3.5f, 0), //offset inside capsule - purely cosmetic
                this.keyboardManager);

            for (int i = 0; i < 100; i++)
            {
                #region NPC Respawn Position Definition
                int spawnPoint = ran.Next(0, 4);
                int pos = 0;
                int posZ = 0;

                int posX2 = ran.Next(-600, 700);
                int posZ1 = ran.Next(-700, -600);
                int posZ2 = ran.Next(900, 1000);

                int posX1 = ran.Next(-800, -700);
                int posX3 = ran.Next(700, 800);
                int posZ3 = ran.Next(-600, 500);


                if (spawnPoint == 0) // spawn at bottom
                {
                    pos = posX2;
                    posZ = posZ1;
                }
                if (spawnPoint == 1) // spawn at top
                {
                    pos = posX2;
                    posZ = posZ2;
                }
                if (spawnPoint == 2) // spawn at right
                {
                    pos = posX1;
                    posZ = posZ3;
                }
                if (spawnPoint == 3) // spawn at left
                {
                    pos = posX3;
                    posZ = posZ3;
                }
                int rotY = ran.Next(-180, 180); // character direction

                #endregion

                cloned = animatedHeroPlayerObject.Clone2();
                cloned.ID += cloneCount;
                cloned.Transform = new Transform3D(new Vector3(pos, 5, posZ),
                new Vector3(-90, rotY, 0), //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                 0.15f * Vector3.One,
                 -Vector3.UnitZ, Vector3.UnitY);

                cloned.Enable(false, AppData.PlayerMass);

                string takeName = "Take 001";
                string fileNameNoSuffix = "dude";
                cloned.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);

                //set the start animtion
                cloned.SetAnimation("Take 001", "dude"); //basically take name (default from 3DS Max) and FBX file name with no suffix

                this.objectManager.Add(cloned);
                Player.citizen++;
            }

        }

        private void InitializeCollidableHeroPlayerObject()
        {
            Transform3D transform = new Transform3D(new Vector3(0, 25, 20),
                new Vector3(90, 0, 0),
                new Vector3(1, 3.5f, 1), -Vector3.UnitZ, Vector3.UnitY);

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            //make the hero a field since we need to point the third person camera controller at this object
            this.heroPlayerObject = new HeroPlayerObject(AppData.PlayerOneID,
                AppData.PlayerOneProgressControllerID, //used to increment/decrement progress on pickup, win, or lose
                ActorType.Player, transform,
                effectParameters,
                this.modelDictionary["cylinder"],
                AppData.PlayerTwoMoveKeys,
                3, 6,
                1.8f, 1.7f,
                AppData.PlayerJumpHeight,
                new Vector3(0, 3, 0),
                this.keyboardManager);
            this.heroPlayerObject.Enable(false, AppData.PlayerMass);

            //don't forget to add it - or else we wont see it!
            this.objectManager.Add(this.heroPlayerObject);
        }

        //skybox is a non-collidable series of ModelObjects with no lighting
        private void InitializeNonCollidableSkyBox(int worldScale)
        {
            //first we will create a prototype plane and then simply clone it for each of the skybox decorator elements (e.g. ground, front, top etc). 
            Transform3D transform = new Transform3D(new Vector3(0, 0, 0), new Vector3(worldScale, 1, worldScale));

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.UnlitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            //create a archetype to use for cloning
            ModelObject planePrototypeModelObject = new ModelObject("plane1", ActorType.Decorator, transform, effectParameters, this.modelDictionary["plane1"]);

            //will be re-used for all planes
            ModelObject clonePlane = null;

            #region Skybox
            //add the back skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["back"];
            //rotate the default plane 90 degrees around the X-axis (use the thumb and curled fingers of your right hand to determine +ve or -ve rotation value)
            clonePlane.Transform.Rotation = new Vector3(90, 0, 0);

            /*
             * Move the plane back to meet with the back edge of the grass (by based on the original 3DS Max model scale)
             * Note:
             * - the interaction between 3DS Max and XNA units which result in the scale factor used below (i.e. 1 x 2.54 x worldScale)/2
             * - that I move the plane down a little on the Y-axiz, purely for aesthetic purposes
             */
            clonePlane.Transform.Translation = new Vector3(0, 0, (-2.54f * worldScale) / 2.0f);
            this.objectManager.Add(clonePlane);

            //As an exercise the student should add the remaining 4 skybox planes here by repeating the clone, texture assignment, rotation, and translation steps above...
            //add the left skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["left"];
            clonePlane.Transform.Rotation = new Vector3(90, 90, 0);
            clonePlane.Transform.Translation = new Vector3((-2.54f * worldScale) / 2.0f, 0, 0);
            this.objectManager.Add(clonePlane);

            //add the right skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["right"];
            clonePlane.Transform.Rotation = new Vector3(90, -90, 0);
            clonePlane.Transform.Translation = new Vector3((2.54f * worldScale) / 2.0f, 0, 0);
            this.objectManager.Add(clonePlane);

            //add the top skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["sky"];
            //notice the combination of rotations to correctly align the sky texture with the sides
            clonePlane.Transform.Rotation = new Vector3(180, -90, 0);
            clonePlane.Transform.Translation = new Vector3(0, ((2.54f * worldScale) / 2.0f), 0);
            this.objectManager.Add(clonePlane);

            //add the front skybox plane
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["front"];
            clonePlane.Transform.Rotation = new Vector3(-90, 0, 180);
            clonePlane.Transform.Translation = new Vector3(0, 0, (2.54f * worldScale) / 2.0f);
            this.objectManager.Add(clonePlane);
            #endregion
        }

        //tree is a non-collidable ModelObject (i.e. in final game the player wont ever reach the far-distance tree) with no-lighting
        private void InitializeNonCollidableFoliage(int worldScale)
        {
            //first we will create a prototype plane and then simply clone it for each of the decorator elements (e.g. trees etc). 
            Transform3D transform = new Transform3D(new Vector3(0, 0, 0), new Vector3(worldScale, 1, worldScale));

            //clone the dictionary effect and set unique properties for the hero player object
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            //a fix to ensure that any image containing transparent pixels will be sent to the correct draw list in ObjectManager
            effectParameters.Alpha = 0.99f;

            ModelObject planePrototypeModelObject = new ModelObject("plane1", ActorType.Decorator, transform, effectParameters, this.modelDictionary["plane1"]);

            //will be re-used for all planes
            ModelObject clonePlane = null;

            //tree
            clonePlane = (ModelObject)planePrototypeModelObject.Clone();
            clonePlane.EffectParameters.Texture = this.textureDictionary["tree2"];
            clonePlane.Transform.Rotation = new Vector3(90, 0, 0);
            /*
             * ISRoT - Scale operations are applied before rotation in XNA so to make the tree tall (i.e. 10) we actually scale 
             * along the Z-axis (remember the original plane is flat on the XZ axis) and then flip the plane to stand upright.
             */
            clonePlane.Transform.Scale = new Vector3(5, 1, 10);
            //y-displacement is (10(XNA) x 2.54f(3DS Max))/2 = 12.7f
            clonePlane.Transform.Translation = new Vector3(0, ((clonePlane.Transform.Scale.Z * 2.54f) / 2), -20);
            this.objectManager.Add(clonePlane);
        }

        //the ground is simply a large flat box with a Box primitive collision surface attached
        private void InitializeCollidableGround(int worldScale)
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;

            /*
             * Note that if we use DualTextureEffectParameters then (a) we must create a model (i.e. box2.fbx) in 3DS Max with two texture channels (i.e. use Unwrap UVW twice)
             * because each texture (diffuse and lightmap) requires a separate set of UV texture coordinates, and (b), this effect does NOT allow us to set up lighting. 
             * Why? Well, we don't need lighting because we can bake a static lighting response into the second texture (the lightmap) in 3DS Max).
             * 
             * See https://knowledge.autodesk.com/support/3ds-max/learn-explore/caas/CloudHelp/cloudhelp/2016/ENU/3DSMax/files/GUID-37414F9F-5E33-4B1C-A77F-547D0B6F511A-htm.html
             * See https://www.youtube.com/watch?v=vuHdnxkXpYo&t=453s
             * See https://www.youtube.com/watch?v=AqiNpRmENIQ&t=1892s
             * 
             */
            Model model = this.modelDictionary["box2"];

            //a simple dual texture demo - dual textures can be used with a lightMap from 3DS Max using the Render to Texture setting
            EffectParameters effectParameters = this.effectDictionary[AppData.UnlitModelsEffectID].Clone() as EffectParameters;
            effectParameters.Texture = this.textureDictionary["newGround"];

            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(worldScale, 0.001f, worldScale), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new CollidableObject("ground", ActorType.CollidableGround, transform3D, effectParameters, model);
            collidableObject.AddPrimitive(new JigLibX.Geometry.Plane(transform3D.Up, transform3D.Translation), new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1); //change to false, see what happens.

            this.objectManager.Add(collidableObject);

            Model model2 = this.modelDictionary["roadCollision"];

            //a simple dual texture demo - dual textures can be used with a lightMap from 3DS Max using the Render to Texture setting
            EffectParameters effectParameters2 = this.effectDictionary[AppData.UnlitModelsEffectID].Clone() as EffectParameters;
            effectParameters2.Alpha = 0;
            transform3D = new Transform3D(Vector3.Zero, Vector3.Zero, new Vector3(29.5f, 1, 29.5f), Vector3.UnitX, Vector3.UnitY);

            TriangleMeshObject roadCollision = new TriangleMeshObject("roadCollision", ActorType.CollidableArchitecture, transform3D, effectParameters2, model2, new MaterialProperties());
            roadCollision.Enable(false, 1); //change to false, see what happens.

            this.objectManager.Add(roadCollision);

        }

        //Triangle mesh objects wrap a tight collision surface around complex shapes - the downside is that TriangleMeshObjects CANNOT be moved
        private void InitializeStaticCollidableTriangleMeshObjects()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-50, 10, 0), new Vector3(45, 45, 0), 0.1f * Vector3.One, Vector3.UnitX, Vector3.UnitY);
            //clone the dictionary effect and set unique properties for the hero player object

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["ml"];
            effectParameters.DiffuseColor = Color.White;
            effectParameters.SpecularPower = 32; //pow((N, H), SpecularPower)
            effectParameters.EmissiveColor = Color.Red;

            CollidableObject collidableObject = new TriangleMeshObject("torus", ActorType.CollidableProp, transform3D, effectParameters,
                            this.modelDictionary["torus"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        //Demos use of a low-polygon model to generate the triangle mesh collision skin - saving CPU cycles on CDCR checking
        private void InitializeStaticCollidableMediumPolyTriangleMeshObjects()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-30, 3, 0),
                new Vector3(0, 0, 0), 0.08f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            CollidableObject collidableObject = new TriangleMeshObject("teapot", ActorType.CollidableProp, transform3D, effectParameters,
                        this.modelDictionary["teapot"], this.modelDictionary["teapot_mediumpoly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        //Demos use of a low-polygon model to generate the triangle mesh collision skin - saving CPU cycles on CDCR checking
        private void InitializeStaticCollidableLowPolyTriangleMeshObjects()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-10, 3, 0),
                new Vector3(0, 0, 0), 0.08f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            //lets set the diffuse color also, for fun.
            effectParameters.DiffuseColor = Color.Blue;

            CollidableObject collidableObject = new TriangleMeshObject("teapot", ActorType.CollidableProp, transform3D, effectParameters,
                this.modelDictionary["teapot"], this.modelDictionary["teapot_lowpoly"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        //if you want objects to be collidable AND moveable then you must attach either a box, sphere, or capsule primitives to the object
        private void InitializeDynamicCollidableObjects()
        {
            CollidableObject collidableObject, archetypeCollidableObject = null;
            Model model = null;

            #region Spheres
            model = this.modelDictionary["sphere"];
            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];

            //make once then clone
            archetypeCollidableObject = new CollidableObject("sphere ", ActorType.CollidablePickup, Transform3D.Zero, effectParameters, model);

            for (int i = 0; i < 10; i++)
            {
                collidableObject = (CollidableObject)archetypeCollidableObject.Clone();

                collidableObject.ID += i;
                collidableObject.Transform = new Transform3D(new Vector3(-50, 100 + 10 * i, i), new Vector3(0, 0, 0),
                    0.082f * Vector3.One, //notice theres a certain amount of tweaking the radii with reference to the collision sphere radius of 2.54f below
                    Vector3.UnitX, Vector3.UnitY);

                collidableObject.AddPrimitive(new Sphere(collidableObject.Transform.Translation, 2.54f), new MaterialProperties(0.2f, 0.8f, 0.7f));
                collidableObject.Enable(false, 1);
                this.objectManager.Add(collidableObject);
            }
            #endregion

            #region Box
            model = this.modelDictionary["box2"];
            effectParameters = (this.effectDictionary[AppData.LitModelsEffectID] as BasicEffectParameters).Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["crate2"];
            //make once then clone
            archetypeCollidableObject = new CollidableObject("box - ", ActorType.CollidablePickup, Transform3D.Zero, effectParameters, model);

            int count = 0;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    collidableObject = (CollidableObject)archetypeCollidableObject.Clone();
                    collidableObject.ID += count;
                    count++;

                    collidableObject.Transform = new Transform3D(new Vector3(25 + 5 * j, 15 + 10 * i, 0), new Vector3(0, 0, 0), new Vector3(2, 4, 1), Vector3.UnitX, Vector3.UnitY);
                    collidableObject.AddPrimitive(new Box(collidableObject.Transform.Translation, Matrix.Identity, /*important do not change - cm to inch*/2.54f * collidableObject.Transform.Scale), new MaterialProperties(0.2f, 0.8f, 0.7f));

                    //increase the mass of the boxes in the demo to see how collidable first person camera interacts vs. spheres (at mass = 1)
                    collidableObject.Enable(false, 1);
                    this.objectManager.Add(collidableObject);
                }
            }

            #endregion
        }

        //demo of a non-collidable ModelObject with attached third person controller
        private void InitializeNonCollidableDriveableObject()
        {
            //place the drivable model to the left of the existing models and specify that forward movement is along the -ve z-axis
            Transform3D transform = new Transform3D(new Vector3(-10, 5, 25), -Vector3.UnitZ, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["crate1"];
            effectParameters.DiffuseColor = Color.Gold;

            //initialise the drivable model object - we've made this variable a field to allow it to be visible to the rail camera controller - see InitializeCameras()
            this.drivableBoxObject = new ModelObject("drivable box1", ActorType.Player, transform, effectParameters, this.modelDictionary["box2"]);

            //attach a DriveController
            drivableBoxObject.AttachController(new DriveController("driveController1", ControllerType.Drive,
                AppData.PlayerTwoMoveKeys, AppData.PlayerMoveSpeed, AppData.PlayerStrafeSpeed, AppData.PlayerRotationSpeed,
                this.managerParameters));

            //add to the objectManager so that it will be drawn and updated
            this.objectManager.Add(drivableBoxObject);
        }

        //demo of some semi-transparent non-collidable ModelObjects
        private void InitializeNonCollidableDecoratorObjects()
        {
            //position the object
            Transform3D transform = new Transform3D(new Vector3(0, 5, 0), Vector3.Zero, Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["crate1"];
            effectParameters.DiffuseColor = Color.Gold;
            effectParameters.Alpha = 0.5f;

            //initialise the boxObject
            ModelObject boxObject = new ModelObject("some box 1", ActorType.Decorator, transform, effectParameters, this.modelDictionary["box2"]);
            //add to the objectManager so that it will be drawn and updated
            this.objectManager.Add(boxObject);

            //a clone variable that we can reuse
            ModelObject clone = null;

            //add a clone of the box model object to test the clone
            clone = (ModelObject)boxObject.Clone();
            clone.Transform.Translation = new Vector3(5, 5, 0);
            //scale it to make it look different
            clone.Transform.Scale = new Vector3(1, 4, 1);
            //change its color
            clone.EffectParameters.DiffuseColor = Color.Red;
            this.objectManager.Add(clone);

            //add more clones here...
        }

        private void InitializeTrees()
        {
            CollidableObject collidableObject, archetypeCollidableObject = null;
            Model model = this.modelDictionary["Tree"];

            Transform3D transform3D = new Transform3D(new Vector3(-100, 0, 0),
                new Vector3(0, 90, 0), new Vector3(2, 6, 2), Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["treeTexture"];
            //effectParameters.Texture = this.textureDictionary["DB2X2_L01"];
            //effectParameters.Texture = this.textureDictionary["DB2X2_L01_Spec"];

            //make once then clone
            archetypeCollidableObject = new CollidableObject("tree1 -", ActorType.CollidableArchitecture, Transform3D.Zero, effectParameters, model);

            int count = 0;
            for (int i = 0; i < 3; i++)
            {
                //if (i == 1 || i == 4)

                for (int j = 0; j < 5; j++)
                {
                    if (i < 3 && j < 3)
                    {
                        collidableObject = (CollidableObject)archetypeCollidableObject.Clone();
                        collidableObject.ID += count;
                        count++;

                        collidableObject.Transform = new Transform3D(new Vector3(-450 + 250 * j, 0, -550 + 300 * i), new Vector3(0, 90, 0), new Vector3(4, 4, 4), Vector3.UnitX, Vector3.UnitY);
                        collidableObject.AddPrimitive(new Box(collidableObject.Transform.Translation, Matrix.Identity, /*important do not change - cm to inch*/  collidableObject.Transform.Scale), new MaterialProperties(1.0f, 1.0f, 1.0f));

                        //increase the mass of the boxes in the demo to see how collidable first person camera interacts vs. spheres (at mass = 1)
                        collidableObject.Enable(true, 1);
                        this.objectManager.Add(collidableObject);
                    }
                    if (i == 4)
                    {
                        collidableObject = (CollidableObject)archetypeCollidableObject.Clone();
                        collidableObject.ID += count;
                        count++;

                        collidableObject.Transform = new Transform3D(new Vector3(-450 + 250 * j, 0, -550 + 220 * i), new Vector3(0, 90, 0), new Vector3(3, 4, 3), Vector3.UnitX, Vector3.UnitY);
                        collidableObject.AddPrimitive(new Box(collidableObject.Transform.Translation, Matrix.Identity, /*important do not change - cm to inch*/  collidableObject.Transform.Scale), new MaterialProperties(1.0f, 1.0f, 1.0f));

                        //increase the mass of the boxes in the demo to see how collidable first person camera interacts vs. spheres (at mass = 1)
                        collidableObject.Enable(true, 1);
                        this.objectManager.Add(collidableObject);
                    }
                }
            }

            Model model2 = this.modelDictionary["tree2"];
            Transform3D transform = new Transform3D(new Vector3(550, 0, -520),
                new Vector3(0,180, 0), new Vector3(0.5f, 0.5f, 0.5f), Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters2 = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters2.Texture = this.textureDictionary["tree2texture"];

            ModelObject modelObject = new ModelObject("tree2", ActorType.Player, transform, effectParameters2, model2);

            this.objectManager.Add(modelObject);



        }

        private void InitializePlayerTargetAndCursor()
        {
            Transform3D transform = new Transform3D(new Vector3(-10, 0, 25), Vector3.UnitZ, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["crate1"];
            effectParameters.DiffuseColor = Color.Gold;

            //initialise the drivable model object - we've made this variable a field to allow it to be visible to the rail camera controller - see InitializeCameras()

            testbox = new PlayerCameraTarget(
                "player Target",
                ActorType.Player,
                transform,
                StatusType.Update);

            //add to the objectManager so that it will be drawn and updated
            this.objectManager.Add(testbox);

            Transform3D cursorTransform = new Transform3D(new Vector3(-100, 0, 25), -Vector3.UnitZ, Vector3.UnitY);

            worldCursor = new WorldCursor("world cursor", ActorType.Player, cursorTransform, effectParameters, this.modelDictionary["box2"], new MaterialProperties());
            worldCursor.Enable(false, 1);
            this.objectManager.Add(worldCursor);
        }

        private void InitializeWallsFences()
        {
            Transform3D transform3D = new Transform3D(new Vector3(-140, 0, -14),
                new Vector3(0, -90, 0), 0.4f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["wall"];

            CollidableObject collidableObject = new TriangleMeshObject("wall1", ActorType.CollidableArchitecture, transform3D,
                            effectParameters, this.modelDictionary["wall"], new MaterialProperties(0.2f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        private void InitializeVideoDisplay()
        {

            BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
            effectParameters.Texture = this.textureDictionary["checkerboard"];
            //make the screen really shiny
            effectParameters.SpecularPower = 256;

            //put the display up on the Y-axis, obviously we can rotate by setting transform3D.Rotation
            Transform3D transform3D = new Transform3D(new Vector3(0, 20, 0), new Vector3(16, 10, 0.1f));

            /* 
             * Does the display need to be collidable? if so use a CollidableObject and not a ModelObject.
             * Notice we dont pass in a texture since the video controller will set this.
             */
            ModelObject videoModelObject = new ModelObject(AppData.VideoIDMainHall, ActorType.Decorator,
                transform3D, effectParameters, this.modelDictionary["box"]);

            ////create the controller
            VideoController videoController = new VideoController(AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, ControllerType.Video, this.eventDispatcher,
                this.textureDictionary["checkerboard"], this.videoDictionary["sample"], 0.5f);

            //make it rotate like a commercial video display
            videoModelObject.AttachController(new RotationController(AppData.VideoIDMainHall + " rotation " + AppData.ControllerIDSuffix,
                ControllerType.Rotation, new Vector3(0, 0.02f, 0)));
            //attach
            videoModelObject.AttachController(videoController);
            //add to object manager
            this.objectManager.Add(videoModelObject);

        }

        #endregion

        #region Initialize Cameras
        private void InitializeCamera(Integer2 screenResolution, string id, Viewport viewPort, Transform3D transform, IController controller, float drawDepth)
        {
            Camera3D camera = new Camera3D(id, ActorType.Camera, transform, ProjectionParameters.StandardDeepSixteenNine, viewPort, drawDepth, StatusType.Update);

            if (controller != null)
                camera.AttachController(controller);

            this.cameraManager.Add(camera);
        }

        private void InitializeMultiScreenCameraDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "";
            int cameraHeight = 5;

            //non-collidable camera 1
            id = "non-collidable FPC 1";
            viewportDictionaryKey = "column1 row0";
            transform = new Transform3D(new Vector3(0, cameraHeight, 10), -Vector3.UnitZ, Vector3.UnitY);
            controller = new FirstPersonCameraController(id + " controller", ControllerType.FirstPerson, AppData.CameraMoveKeys, AppData.CameraMoveSpeed, AppData.CameraStrafeSpeed, AppData.CameraRotationSpeed,
                this.managerParameters);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 1
            id = "non-collidable security 1";
            viewportDictionaryKey = "column0 row0";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 60, AppData.SecurityCameraRotationSpeedSlow, AppData.SecurityCameraRotationAxisYaw);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 2
            id = "non-collidable security 2";
            viewportDictionaryKey = "column0 row1";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 45, AppData.SecurityCameraRotationSpeedMedium, new Vector3(1, 1, 0));
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //security camera 3
            id = "non-collidable security 3";
            viewportDictionaryKey = "column0 row2";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new SecurityCameraController(id + " controller", ControllerType.Security, 30, AppData.SecurityCameraRotationSpeedFast, new Vector3(4, 1, 0));
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //track camera 1
            id = "non-collidable track 1";
            viewportDictionaryKey = "column0 row3";
            transform = new Transform3D(new Vector3(0, cameraHeight, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["unique curve name 1"], PlayStatusType.Play);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //rail camera 1
            id = "non-collidable rail 1";
            viewportDictionaryKey = "column0 row4";
            //since the camera will be set on a rail it doesnt matter what the initial transform is set to
            transform = Transform3D.Zero;

            //track animated player if it's available
            if (this.animatedHeroPlayerObject != null)
                controller = new RailController(id + " controller", ControllerType.Rail, this.animatedHeroPlayerObject, this.railDictionary["rail1 - parallel to x-axis"]);
            else
                controller = new RailController(id + " controller", ControllerType.Rail, this.drivableBoxObject, this.railDictionary["rail1 - parallel to x-axis"]);

            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);
        }

        private void InitializeCollidableFirstPersonDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            string id = "";
            string viewportDictionaryKey = "";
            float drawDepth = 0;

            id = "collidable first person camera";
            viewportDictionaryKey = "full viewport";
            //doesnt matter how high on Y-axis we start the camera since it's collidable and will fall until the capsule toches the ground plane - see AppData::CollidableCameraViewHeight
            //just ensure that the Y-axis height is slightly more than AppData::CollidableCameraViewHeight otherwise the player will rise eerily upwards at the start of the game
            //as the CDCR system pushes the capsule out of the collidable ground plane 
            transform = new Transform3D(new Vector3(0, 1.1f * AppData.CollidableCameraViewHeight, 60), -Vector3.UnitZ, Vector3.UnitY);

            Camera3D camera = new Camera3D(id, ActorType.Camera, transform,
                    ProjectionParameters.StandardDeepSixteenNine, this.viewPortDictionary[viewportDictionaryKey], drawDepth, StatusType.Off);

            //attach a CollidableFirstPersonController
            camera.AttachController(new CollidableFirstPersonCameraController(
                    camera + " controller",
                    ControllerType.CollidableFirstPerson,
                    AppData.CameraMoveKeys,
                    AppData.CollidableCameraMoveSpeed, AppData.CollidableCameraStrafeSpeed, AppData.CameraRotationSpeed,
                    this.managerParameters,
                    camera, //parent
                    AppData.CollidableCameraCapsuleRadius,
                    AppData.CollidableCameraViewHeight,
                    1, 1, //accel, decel
                    AppData.CollidableCameraMass,
                    AppData.CollidableCameraJumpHeight,
                    Vector3.Zero)); //translation offset
            this.cameraManager.Add(camera);

        }

        private void InitializeBoxCamera(Integer2 screenResolution)
        {
            Transform3D transform = null;
            string id = "";
            string viewportDictionaryKey = "";
            float drawDepth = 0;

            id = "player camera";
            viewportDictionaryKey = "full viewport";
            //doesnt matter how high on Y-axis we start the camera since it's collidable and will fall until the capsule toches the ground plane - see AppData::CollidableCameraViewHeight
            //just ensure that the Y-axis height is slightly more than AppData::CollidableCameraViewHeight otherwise the player will rise eerily upwards at the start of the game
            //as the CDCR system pushes the capsule out of the collidable ground plane 
            transform = new Transform3D(new Vector3(0, 1.1f * AppData.CollidableCameraViewHeight, 60), -Vector3.UnitZ, Vector3.UnitY);

            Camera3D camera = new Camera3D(id, ActorType.Camera, transform,
                    ProjectionParameters.StandardDeepSixteenNine, this.viewPortDictionary[viewportDictionaryKey], drawDepth, StatusType.Update);

            //attach a CollidableFirstPersonController
            camera.AttachController(new ThirdPersonController("box camera",

                ControllerType.ThirdPerson, this.testbox, 500.0f, 0.05f, 40.0f, 0.05f, 0.5f, 0f, this.mouseManager, this.keyboardManager, this.soundManager));

            worldCursor.AttachController(new MouseToGroundController("mouse to ground", ControllerType.Track, camera, mouseManager));

            this.cameraManager.Add(camera);
        }

        private void InitializeIntroCamera()
        {
            Transform3D transform = null;
            string id = "";
            string viewportDictionaryKey = "";
            float drawDepth = 0;
            #region Track Camera
            id = "Intro Camera";
            viewportDictionaryKey = "full viewport";
            //transform
            transform = new Transform3D(Vector3.Zero, Vector3.Zero, Vector3.One, -Vector3.UnitZ, Vector3.UnitY);
            //camera
            Camera3D camera = new Camera3D(id, ActorType.Camera, transform,
                    ProjectionParameters.StandardDeepSixteenNine, this.viewPortDictionary[viewportDictionaryKey], drawDepth, StatusType.Off, eventDispatcher );
            //transform 3D
            Transform3DCurve transform3DCurve = new Transform3DCurve(CurveLoopType.Linear);

            transform3DCurve.Add(new Vector3(-620, 50, -700), Vector3.UnitZ, Vector3.UnitY, 0);
            transform3DCurve.Add(new Vector3(-880, 50, -500), Vector3.UnitZ, Vector3.UnitY, 5);
            transform3DCurve.Add(new Vector3(-820, 50, 0), Vector3.UnitX, Vector3.UnitY, 8);
            transform3DCurve.Add(new Vector3(-620, 50, -620), Vector3.UnitX, Vector3.UnitY, 12);
            transform3DCurve.Add(new Vector3(-620, 500, -620), -Vector3.UnitY, Vector3.UnitY, 15);

            //controller
            camera.AttachController(new CustomCurveController("Intro Camera", ControllerType.CollidableFirstPerson, transform3DCurve, PlayStatusType.Play, this.cameraManager));

            this.cameraManager.Add(camera);

            #endregion
        }

        //adds three camera from 3 different perspectives that we can cycle through
        private void InitializeSingleScreenCycleableCameraDemo(Integer2 screenResolution)
        {

            InitializeCollidableFirstPersonDemo(screenResolution);
            InitializeBoxCamera(screenResolution);

            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "full viewport";

            //track camera 1
            id = "non-collidable track 1";
            transform = new Transform3D(new Vector3(0, 0, 20), -Vector3.UnitZ, Vector3.UnitY);
            controller = new CurveController(id + " controller", ControllerType.Track, this.curveDictionary["unique curve name 1"], PlayStatusType.Play);
            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

            //rail camera 1
            id = "non-collidable rail 1";
            //since the camera will be set on a rail it doesnt matter what the initial transform is set to
            transform = Transform3D.Zero;

            //track animated player if it's available
            if (this.animatedHeroPlayerObject != null)
                controller = new RailController(id + " controller", ControllerType.Rail, this.animatedHeroPlayerObject, this.railDictionary["rail1 - parallel to x-axis"]);
            else
                controller = new RailController(id + " controller", ControllerType.Rail, this.drivableBoxObject, this.railDictionary["rail1 - parallel to x-axis"]);


            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

        }

        //adds a third person looking at collidable HeroPlayerObject
        private void InitializeCollidableThirdPersonDemo(Integer2 screenResolution)
        {
            Transform3D transform = null;
            IController controller = null;
            string id = "";
            string viewportDictionaryKey = "full viewport";

            //track camera 1
            id = "box camera";
            //doesnt matter since it will reset based on target actor data
            transform = Transform3D.Zero;

            if (this.testbox != null)
            {
                controller = new ThirdPersonController(id + "ctrllr", ControllerType.ThirdPerson, this.testbox,
                AppData.CameraThirdPersonDistance, AppData.CameraThirdPersonScrollSpeedDistanceMultiplier,
                AppData.CameraThirdPersonElevationAngleInDegrees, AppData.CameraThirdPersonScrollSpeedElevatationMultiplier,
                LerpSpeed.Fast, LerpSpeed.Medium, this.mouseManager, this.keyboardManager, this.soundManager);
            }
            else
            {
                controller = new ThirdPersonController(id + "ctrllr", ControllerType.ThirdPerson, this.testbox,
                AppData.CameraThirdPersonDistance, AppData.CameraThirdPersonScrollSpeedDistanceMultiplier,
                AppData.CameraThirdPersonElevationAngleInDegrees, AppData.CameraThirdPersonScrollSpeedElevatationMultiplier,
                LerpSpeed.Fast, LerpSpeed.Medium, this.mouseManager, this.keyboardManager, this.soundManager);
            }



            InitializeCamera(screenResolution, id, this.viewPortDictionary[viewportDictionaryKey], transform, controller, 0);

        }

        #region Switch Camera
        private bool IsCamera(Camera3D camera, string serachID)
        {
            return camera.ID.Equals(serachID);
        }

        //private bool IsCamera(Camera3D camera)
        //{
        //    return camera.ID.Equals(" camera 1");
        //}

        private void DemoCameraManager()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.Q))
            {
                this.cameraManager.CycleCamera();

                if (this.cameraManager.ActiveCamera.ID == "player camera")
                {
                    //System.Diagnostics.Debug.WriteLine(this.player.buildingAboutToBeBuilt);
                    if (this.player.buildingAboutToBeBuilt != null)
                    {
                        //System.Diagnostics.Debug.WriteLine("Play");
                        this.player.buildingAboutToBeBuilt.ControllerList.Find((controller) => (controller.GetID() == "mouse to ground")).SetControllerPlayStatus(PlayStatusType.Play);
                    }
                }
                else
                {
                    if (this.player.buildingAboutToBeBuilt != null)
                    {
                        //System.Diagnostics.Debug.WriteLine("Stop");
                        this.player.buildingAboutToBeBuilt.ControllerList.Find((controller) => (controller.GetID() == "mouse to ground")).SetControllerPlayStatus(PlayStatusType.Stop);
                    }
                }
            }

            //int findIndex = this.cameraManager.SetActiveCameraBy((camera) => (camera.ID.Equals("non-existent ID")), 0);

            //System.Diagnostics.Debug.WriteLine(findIndex);


            //this.cameraManager.Remove((camera) => (camera.ID == "Intro Camera"));
        }
        #endregion

        #endregion


        #region MoveableBox
        Vector3 driveRotation = Vector3.Zero;
        private WorldCursor worldCursor;
        private Player player;
        private void MoveableCharacter(GameTime gameTime)
        {
            KeyboardState ksState = Keyboard.GetState();
            if (ksState.IsKeyDown(Keys.Up)) //forward
            {
                this.testbox.Transform.TranslateBy(
                    this.testbox.Transform.Look * gameTime.ElapsedGameTime.Milliseconds
                    * 0.05f);

            }
            else if (ksState.IsKeyDown(Keys.Down))
            {
                this.testbox.Transform.TranslateBy(
                    -this.testbox.Transform.Look * gameTime.ElapsedGameTime.Milliseconds
                    * 0.05f);
            }
            if (ksState.IsKeyDown(Keys.Left)) //left
            {
                this.testbox.Transform.RotateAroundYBy(1);
            }
            else if (ksState.IsKeyDown(Keys.Right))
            {
                this.testbox.Transform.RotateAroundYBy(-1);

            }
            if (ksState.IsKeyDown(Keys.Z))
            {

            }
        }
        #endregion


        #region Events
        private void InitializeEventDispatcher()
        {
            //initialize with an arbitrary size based on the expected number of events per update cycle, increase/reduce where appropriate
            this.eventDispatcher = new EventDispatcher(this, 20);

            //dont forget to add to the Component list otherwise EventDispatcher::Update won't get called and no event processing will occur!
            Components.Add(this.eventDispatcher);
        }

        private void StartGame()
        {
            //will be received by the menu manager and screen manager and set the menu to be shown and game to be paused
            EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.MainMenu));

            //publish an event to set the camera
            object[] additionalEventParamsB = { "collidable first person camera 1" };
            EventDispatcher.Publish(new EventData(EventActionType.OnCameraSetActive, EventCategoryType.Camera, additionalEventParamsB));
            //we could also just use the line below, but why not use our event dispatcher?
            //this.cameraManager.SetActiveCamera(x => x.ID.Equals("collidable first person camera 1"));

        }
        #endregion

        #region Menu & UI
        private void AddMenuElements()
        {
            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            UIButtonObject uiButtonObject = null, clone = null;
            string sceneID = "", buttonID = "", buttonText = "";
            int verticalBtnSeparation = 90;

            #region Main Menu

            sceneID = "main menu";

            //retrieve the background texture
            //texture = this.textureDictionary["mainmenu"];
            texture = this.textureDictionary["start_menu"];
            //scale the texture to fit the entire screen
            Vector2 scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);



            this.menuManager.Add(sceneID, new UITextureObject("mainmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));


            //add start button
            buttonID = "startbtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 360);
            texture = this.textureDictionary["startButton"];
            transform = new Transform2D(position,
                0, new Vector2(0.5f, 0.5f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));


            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);




            //add help Button
            buttonID = "helpbtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 360 + verticalBtnSeparation);
            texture = this.textureDictionary["helpBtn"];
            transform = new Transform2D(position,
                0, new Vector2(0.5f, 0.5f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));


            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);



            //add audio button
            buttonID = "audiobtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 360 + 2 * verticalBtnSeparation);
            texture = this.textureDictionary["optionsButton"];
            transform.Translation = position;

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.ForestGreen, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);


            //add exit button
            buttonID = "exitbtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 360 + (3 * verticalBtnSeparation));
            texture = this.textureDictionary["exitButton"];
            transform.Translation = position;

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);


            ////add audio button - clone the audio button then just reset texture, ids etc in all the clones
            //clone = (UIButtonObject)uiButtonObject.Clone();
            //clone.ID = "audiobtn";
            //clone.Text = "Audio";
            ////move down on Y-axis for next button
            //clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            ////change the texture blend color
            //clone.Color = Color.White;
            //this.menuManager.Add(sceneID, clone);




            ////add controls button - clone the audio button then just reset texture, ids etc in all the clones
            //clone = (UIButtonObject)uiButtonObject.Clone();
            //clone.ID = "controlsbtn";
            ////clone.Text = "Controls";
            ////move down on Y-axis for next button
            //clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            ////change the texture blend color
            //clone.Color = Color.LightBlue;
            //this.menuManager.Add(sceneID, clone);




            ////add exit button - clone the audio button then just reset texture, ids etc in all the clones
            //clone = (UIButtonObject)uiButtonObject.Clone();
            //clone.ID = "exitbtn";
            ////clone.Text = "Exit";
            ////move down on Y-axis for next button
            //clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            ////change the texture blend color
            //clone.Color = Color.LightYellow;
            ////store the original color since if we modify with a controller and need to reset
            //clone.OriginalColor = clone.Color;
            ////attach another controller on the exit button just to illustrate multi-controller approach
            //clone.AttachController(new UIColorSineLerpController("colorSineLerpController", ControllerType.SineColorLerp,
            //        new TrigonometricParameters(1, 0.4f, 0), Color.LightSeaGreen, Color.LightGreen));
            //this.menuManager.Add(sceneID, clone);



            #endregion

            #region help menu

            // help bg
            texture = this.textureDictionary["helpPageBG"];
            transform.Origin = Vector2.Zero;
            transform.Translation = Vector2.Zero;
            transform.Scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            sceneID = "helpMenu";
            this.menuManager.Add(sceneID, new UITextureObject("helpBackground", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add start button
            buttonID = "helpControls";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 360);
            texture = this.textureDictionary["controlBtn"];
            transform = new Transform2D(position,
                0, new Vector2(0.5f, 0.5f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));


            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);




            //add help Button
            buttonID = "helpRules";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 360 + verticalBtnSeparation);
            texture = this.textureDictionary["ruleButton"];
            transform = new Transform2D(position,
                0, new Vector2(0.5f, 0.5f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));


            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);



            //add controls button
            buttonID = "backbtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 360 + 2 * verticalBtnSeparation);
            texture = this.textureDictionary["exitButton"];
            transform.Translation = position;

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.ForestGreen, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);


            #endregion
            verticalBtnSeparation = 125;
            #region Audio Menu
            sceneID = "audio menu";

            //retrieve the audio menu background texture
            texture = this.textureDictionary["audiomenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("audiomenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));





            //add start button
            buttonID = "volumeUpbtn";
            buttonText = "Volume Up";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2.0f, 180);
            texture = this.textureDictionary["blankButton"];
            transform = new Transform2D(position,
                0, new Vector2(0.8f, 0.7f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));


            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, Color.White, SpriteEffects.None, 0.1f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.Black, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.05f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);



            verticalBtnSeparation = verticalBtnSeparation - 10;

            //add volume down button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, verticalBtnSeparation);
            clone.ID = "volumeDownbtn";
            clone.Text = "Volume Down";
            //change the texture blend color
            clone.Color = Color.LightGreen;
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 2 * verticalBtnSeparation);
            clone.ID = "volumeMutebtn";
            clone.Text = "Volume Mute";
            //change the texture blend color
            clone.Color = Color.LightBlue;
            this.menuManager.Add(sceneID, clone);

            //add volume mute button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 3 * verticalBtnSeparation);
            clone.ID = "volumeUnMutebtn";
            clone.Text = "Volume Un-mute";
            //change the texture blend color
            clone.Color = Color.LightSalmon;
            this.menuManager.Add(sceneID, clone);

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 4 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            clone.Color = Color.LightYellow;
            this.menuManager.Add(sceneID, clone);
            #endregion

            #region Controls Menu
            sceneID = "controls menu";

            //retrieve the controls menu background texture
            texture = this.textureDictionary["controlsmenu"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.White, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //add back button - clone the audio button then just reset texture, ids etc in all the clones
            clone = (UIButtonObject)uiButtonObject.Clone();
            //move down on Y-axis for next button
            clone.Transform.Translation += new Vector2(0, 9 * verticalBtnSeparation);
            clone.ID = "backbtn";
            clone.Text = "Back";
            //change the texture blend color
            clone.Color = Color.LightYellow;
            this.menuManager.Add(sceneID, clone);


            sceneID = "helpControls";


            //retrieve the controls menu background texture
            texture = this.textureDictionary["helpPageBG"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.DarkGray, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //retrieve the controls menu background texture
            texture = this.textureDictionary["control"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None,
                0.5f, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            buttonID = "backbtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth * (3f/4f), graphics.PreferredBackBufferHeight * (7f/8f));
            texture = this.textureDictionary["exitButton"];
            
            transform = new Transform2D(position,
                0, new Vector2(0.5f, 0.5f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, "",
                this.fontDictionary["menu"],
                Color.ForestGreen, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);

            sceneID = "helpRules";


            //retrieve the controls menu background texture
            texture = this.textureDictionary["helpPageBG"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)graphics.PreferredBackBufferWidth / texture.Width,
                (float)graphics.PreferredBackBufferHeight / texture.Height);
            transform = new Transform2D(scale);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform, Color.DarkGray, SpriteEffects.None,
                1, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            //retrieve the controls menu background texture
            texture = this.textureDictionary["rule"];
            //scale the texture to fit the entire screen
            scale = new Vector2((float)(graphics.PreferredBackBufferWidth*(4f/5f)) / texture.Width,
                (float)(graphics.PreferredBackBufferHeight * (4f / 5f)) / texture.Height);
            transform = new Transform2D(scale);

            transform.Translation = new Vector2(graphics.PreferredBackBufferWidth / 9f, 100);
            this.menuManager.Add(sceneID, new UITextureObject("controlsmenuTexture", ActorType.UIStaticTexture,
                StatusType.Drawn, //notice we dont need to update a static texture
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None,
                0.5f, //depth is 1 so its always sorted to the back of other menu elements
                texture));

            buttonID = "backbtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth * (3f / 4f), graphics.PreferredBackBufferHeight * (7f / 8f));
            texture = this.textureDictionary["exitButton"];

            transform = new Transform2D(position,
                0, new Vector2(0.5f, 0.5f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform.Clone() as Transform2D, Color.White, SpriteEffects.None, 0.1f, texture, "",
                this.fontDictionary["menu"],
                Color.ForestGreen, new Vector2(0, 2));

            uiButtonObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.1f, 0.2f, 1)));
            this.menuManager.Add(sceneID, uiButtonObject);
            #endregion


            // play menu music
            object[] addParams = { "GazumpMainTheme" };
            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, addParams));
        }



        private void AddUIElements()
        {
            InitializeUIMousePointer();
            InitializePlayerHUD();
        }

        private void InitializePlayerHUD()
        {
            #region Timer Text
            Transform2D transform = null;
            SpriteFont font;
            UITextObject textObject;
            Vector2 position = Vector2.Zero;
            Vector2 scale = Vector2.One;

            font = this.fontDictionary["inGame"];

            position = new Vector2((graphics.PreferredBackBufferWidth / 2.0f) - 240, 10);
            transform = new Transform2D(position, 0, scale,
                Vector2.Zero, /*new Vector2(texture.Width/2.0f, texture.Height/2.0f),*/
                new Integer2(10, 10));

            // timer 
            textObject = new UITextObject("timer colon", ActorType.UIStaticText, StatusType.Drawn | StatusType.Update, (Transform2D)transform.Clone(), Color.Yellow, SpriteEffects.None, 0, ":", font);
            textObject.AttachController(new TimerController("timer controller", ControllerType.UIProgress, this.player));

            this.uiManager.Add(textObject);
            #endregion

            #region Population
            transform.Translation = new Vector2(1000, 10);



            textObject = new UITextObject("timer colon", ActorType.UIStaticText, StatusType.Drawn | StatusType.Update, (Transform2D)transform.Clone(), Color.Yellow, SpriteEffects.None, 0, "0", font);

            //textObject.OriginalColor = textObject.Color;

            textObject.AttachController(new PopulationController("population controller", ControllerType.UIProgress, this.player));


            textObject.AttachController(new UIScaleSineLerpController("sineScaleLerpController2", ControllerType.SineScaleLerp,
              new TrigonometricParameters(0.05f, 0.2f, 1)));




            this.uiManager.Add(textObject);

            #endregion

            #region Restaurant Income
            transform.Translation = new Vector2(810, 10);
            textObject = new UITextObject("Restaurant Income", ActorType.UIStaticText, StatusType.Drawn | StatusType.Update, (Transform2D)transform.Clone(), Color.Yellow, SpriteEffects.None, 0, "0", font);
            textObject.AttachController(new RestaurantIncomeController("income controller", ControllerType.UIProgress));

            this.uiManager.Add(textObject);

            #endregion
            #region Hotel

            transform.Translation = new Vector2(graphics.PreferredBackBufferWidth - 680, 10);
            textObject = new UITextObject("Hotel Income", ActorType.UIStaticText, StatusType.Drawn | StatusType.Update, (Transform2D)transform.Clone(), Color.Yellow, SpriteEffects.None, 0, "0", font);
            textObject.AttachController(new HotelIncomeController("income controller", ControllerType.UIProgress));

            this.uiManager.Add(textObject);

            #endregion

            #region Total Income
            transform.Translation = new Vector2(180, 10);
            textObject = new UITextObject("Total Cash", ActorType.UIStaticText, StatusType.Drawn | StatusType.Update, (Transform2D)transform.Clone(), Color.Yellow, SpriteEffects.None, 0, "0", font);
            textObject.AttachController(new cashController("cash controller", ControllerType.UIProgress, this.eventDispatcher, this.player));

            this.uiManager.Add(textObject);

            #endregion 

            InitializeUIInventoryMenu();

            InitializeUITop();

            InitializeUIHotkeys();

            InitializeGameOverOverlay();
        }

        private void InitializeUIMousePointer()
        {
            Texture2D texture = this.textureDictionary["reticuleDefault"];
            //show complete texture
            Microsoft.Xna.Framework.Rectangle sourceRectangle = new Microsoft.Xna.Framework.Rectangle(0, 0, texture.Width, texture.Height);

            //listens for object picking events from the object picking manager
            UIPickingMouseObject myUIMouseObject = new UIPickingMouseObject("picking mouseObject",
                ActorType.UITexture,
                new Transform2D(Vector2.One),
                this.fontDictionary["mouse"],
                "",
                new Vector2(0, 40),
                texture,
                this.mouseManager,
                this.eventDispatcher);
            this.uiManager.Add(myUIMouseObject);
        }





        private void InitializeUITop()
        {

            Transform2D transform = null;
            Texture2D texture = null;
            UITextureObject textureObject = null;
            UITextureObject textureObject2 = null;
            Vector2 position = Vector2.Zero;
            Vector2 scale = Vector2.Zero;
            float verticalOffset = -5;
            //int startValue;

            texture = this.textureDictionary["HUDTop"];
            scale = new Vector2(0.3152f, 0.24f);

            position = new Vector2(-5, verticalOffset);
            transform = new Transform2D(position, 0, scale,
                Vector2.Zero, /*new Vector2(texture.Width/2.0f, texture.Height/2.0f),*/
                new Integer2(texture.Width, texture.Height));



            textureObject = new UITextureObject(AppData.PlayerOneProgressID,
                    ActorType.UITexture,
                    StatusType.Drawn | StatusType.Update,
                    transform, Color.White,
                    SpriteEffects.None,
                    1,
                    texture);
            this.uiManager.Add(textureObject);

        }


        private void InitializeUIHotkeys()
        {

            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;
            UIButtonObject uiButtonObject = null;
            string sceneID = "inGame", buttonID = "", buttonText = "";
            //int horizontalBtnSeparation = 75;


            buttonID = "houseBtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 5f, 650);
            texture = this.textureDictionary["houseButton"];
            transform = new Transform2D(position,
                0, new Vector2(0.4f, 0.4f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));


            StatusType statusType = StatusType.Update | StatusType.Drawn;

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, statusType,
                transform, Color.White, SpriteEffects.None, 0.9f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));


            this.hotKeyManager.Add(sceneID, uiButtonObject);




            buttonID = "centreBtn";

            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 2f, 650);
            texture = this.textureDictionary["hotelButton"];
            transform = new Transform2D(position,
                0, new Vector2(0.4f, 0.4f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, Color.White, SpriteEffects.None, 0.9f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            //this.uiManager.Add(uiButtonObject);

            this.hotKeyManager.Add(sceneID, uiButtonObject);



            buttonID = "hotelBtn";
            //buttonText = "Start";
            position = new Vector2(graphics.PreferredBackBufferWidth / 1.25f, 650);
            texture = this.textureDictionary["centreButton"];
            transform = new Transform2D(position,
                0, new Vector2(0.4f, 0.4f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));

            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                transform, Color.White, SpriteEffects.None, 0.9f, texture, buttonText,
                this.fontDictionary["menu"],
                Color.DarkGray, new Vector2(0, 2));

            //this.uiManager.Add(uiButtonObject);

            this.hotKeyManager.Add(sceneID, uiButtonObject);


        }






        #endregion

        private void InitializeUIInventoryMenu()
        {
            //throw new NotImplementedException();
        }
        // #endregion

        #region Effects
        private void InitializeEffects()
        {
            BasicEffect basicEffect = null;
            DualTextureEffect dualTextureEffect = null;
            Effect billboardEffect = null;


            #region Lit objects
            //create a BasicEffect and set the lighting conditions for all models that use this effect in their EffectParameters field
            basicEffect = new BasicEffect(graphics.GraphicsDevice);

            basicEffect.TextureEnabled = true;
            basicEffect.PreferPerPixelLighting = true;
            basicEffect.EnableDefaultLighting();
            this.effectDictionary.Add(AppData.LitModelsEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For Unlit objects
            //used for model objects that dont interact with lighting i.e. sky
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = true;
            basicEffect.LightingEnabled = false;
            this.effectDictionary.Add(AppData.UnlitModelsEffectID, new BasicEffectParameters(basicEffect));
            #endregion

            #region For dual texture objects
            dualTextureEffect = new DualTextureEffect(graphics.GraphicsDevice);
            this.effectDictionary.Add(AppData.UnlitModelDualEffectID, new DualTextureEffectParameters(dualTextureEffect));
            #endregion

            #region For unlit billboard objects
            billboardEffect = Content.Load<Effect>("Assets/Effects/billboard");
            this.effectDictionary.Add(AppData.UnlitBillboardsEffectID, new BillboardEffectParameters(billboardEffect));
            #endregion

            #region For unlit primitive objects
            basicEffect = new BasicEffect(graphics.GraphicsDevice);
            basicEffect.TextureEnabled = true;
            basicEffect.VertexColorEnabled = true;
            this.effectDictionary.Add(AppData.UnLitPrimitivesEffectID, new BasicEffectParameters(basicEffect));
            #endregion

        }
        #endregion

        #region Content, Update, Draw        
        protected override void LoadContent()
        {
            //moved to Initialize
            //spriteBatch = new SpriteBatch(GraphicsDevice);

            //            #region Add Menu & UI
            //            InitializeMenu();
            //            AddMenuElements();
            //            InitializeUI();
            //            AddUIElements();
            //            #endregion

            //#if DEBUG
            //            InitializeDebugTextInfo();
            //#endif

        }

        protected override void UnloadContent()
        {
            //formally call garbage collection on all ContentDictionary objects to de-allocate resources from RAM
            this.modelDictionary.Dispose();
            this.textureDictionary.Dispose();
            this.fontDictionary.Dispose();
            this.videoDictionary.Dispose();

        }

        private void InitializeGameOverOverlay()
        {
            UIButtonObject uiButtonObject = null;

            Transform2D transform = null;
            Texture2D texture = null;
            Vector2 position = Vector2.Zero;

            string sceneID = "", buttonID = "endGame", buttonText = "0";


            texture = this.textureDictionary["gameOver"];

            position = new Vector2(graphics.PreferredBackBufferWidth / 2f, (graphics.PreferredBackBufferHeight / 2f));

            transform = new Transform2D(position,
                0, new Vector2(0.4f, 0.4f),
                new Vector2(texture.Width / 2.0f, texture.Height / 2.0f), new Integer2(texture.Width, texture.Height));


            uiButtonObject = new UIButtonObject(buttonID, ActorType.UIButton, StatusType.Update | StatusType.Drawn,
                    transform, Color.White, SpriteEffects.None, 0.9f, texture, buttonText,
                    this.fontDictionary["gameOver"],
                    Color.Yellow, new Vector2(0, 60));

            uiButtonObject.AttachController(new GameOverController("game over", ControllerType.UIProgress, this.eventDispatcher, uiButtonObject, this.player));

            uiButtonObject.StatusType = StatusType.Off;

            this.uiManager.Add(uiButtonObject);

        }


        protected override void Update(GameTime gameTime)
        {
            DemoCameraManager();
            RespawnNewNPCs();
            //if (Player.gameFinished == true)
            //{
            //    this.objectManager.Remove((dude) => (dude.ID == "dude"));
            //    this.objectManager.Remove((dude) => (dude.ID == "PeopleClone -"));
            //}

            //exit using new gamepad manager
            if (this.gamePadManager.IsPlayerConnected(PlayerIndex.One) && this.gamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.Back))
                this.Exit();
#if DEMO
            #region Demo
            demoVideoDisplay();
            demoSoundManager();
            demoCameraChange();
            demoAlphaChange();
            demoUIProgressUpdate();
            demoHideDebugInfo();
            demoGamePadManager();
            #endregion
#endif
            base.Update(gameTime);
        }

        private void RespawnNewNPCs()
        {
            if (Player.citizen <= 95)
            {
                Random ran = new Random();

                Transform3D transform3D = null;

                transform3D = new Transform3D(new Vector3(0, 10, 40),
                    new Vector3(-90, 0, 0), //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                     0.15f * Vector3.One,
                     -Vector3.UnitZ, Vector3.UnitY);

                BasicEffectParameters effectParameters = this.effectDictionary[AppData.LitModelsEffectID].Clone() as BasicEffectParameters;
                //remember we can set diffuse color and alpha too but not specular, emissive, directional lights as I dont read those parameters in ObjectManager::DrawObject() - this was purely a time constraint on my part.
                effectParameters.DiffuseColor = Color.White;
                effectParameters.Alpha = 1;
                //if we dont specify a texture then the object manager will draw using whatever textures were baked into the animation in 3DS Max
                effectParameters.Texture = null;

                DudeAnimatedPlayerObject animatedHeroPlayerObject = new DudeAnimatedPlayerObject("dude",
                        ActorType.Player, transform3D,
                    effectParameters,
                    AppData.SquirrelPlayerMoveKeys,
                    AppData.PlayerRadius,
                    AppData.PlayerHeight,
                    1, 1,  //accel, decel
                    AppData.DudeMoveSpeed,
                    AppData.DudeRotationSpeed,
                    AppData.DudeJumpHeight,
                    new Vector3(0, -3.5f, 0), //offset inside capsule - purely cosmetic
                    this.keyboardManager);

                for (int i = 0; i < 5; i++)
                {
                    #region NPC Respawn Position Definition
                    int spawnPoint = ran.Next(0, 4);
                    int pos = 0;
                    int posZ = 0;

                    int posX2 = ran.Next(-600, 700);
                    int posZ1 = ran.Next(-700, -600);
                    int posZ2 = ran.Next(900, 1000);

                    int posX1 = ran.Next(-800, -700);
                    int posX3 = ran.Next(700, 800);
                    int posZ3 = ran.Next(-600, 500);


                    if (spawnPoint == 0) // spawn at bottom
                    {
                        pos = posX2;
                        posZ = posZ1;
                    }
                    if (spawnPoint == 1) // spawn at top
                    {
                        pos = posX2;
                        posZ = posZ2;
                    }
                    if (spawnPoint == 2) // spawn at right
                    {
                        pos = posX1;
                        posZ = posZ3;
                    }
                    if (spawnPoint == 3) // spawn at left
                    {
                        pos = posX3;
                        posZ = posZ3;
                    }
                    int rotY = ran.Next(-180, 180); // character direction

                    #endregion

                    cloned = animatedHeroPlayerObject.Clone2();
                    cloned.ID += cloneCount;
                    cloned.Transform = new Transform3D(new Vector3(pos, 20, posZ),
                    new Vector3(-90, rotY, 0), //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                     0.15f * Vector3.One,
                     -Vector3.UnitZ, Vector3.UnitY);

                    cloned.Enable(false, AppData.PlayerMass);

                    string takeName = "Take 001";
                    string fileNameNoSuffix = "dude";
                    cloned.AddAnimation(takeName, fileNameNoSuffix, this.modelDictionary[fileNameNoSuffix]);

                    //set the start animtion
                    cloned.SetAnimation("Take 001", "dude"); //basically take name (default from 3DS Max) and FBX file name with no suffix

                    this.objectManager.Add(cloned);
                    Player.citizen++;
                }
            }
        }

        private void demoVideoDisplay()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.F3))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Video, additonalParameters));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.F4))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix };
                EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.Video, additonalParameters));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.Up))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, 0.05f };
                EventDispatcher.Publish(new EventData(EventActionType.OnVolumeUp, EventCategoryType.Video, additonalParameters));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.Down))
            {
                //pass the target ID for the controller to play the right video
                object[] additonalParameters = { AppData.VideoIDMainHall + " video " + AppData.ControllerIDSuffix, 0.05f };
                EventDispatcher.Publish(new EventData(EventActionType.OnMute, EventCategoryType.Video, additonalParameters));
            }


        }
        private void demoGamePadManager()
        {
            if (this.gamePadManager.IsButtonPressed(PlayerIndex.One, Buttons.RightTrigger))
            {
                //do something....
            }
        }
        private void demoSoundManager()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.F2))
            {
                object[] additionalParameters = { "boing" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));

                //stopping a cue
                //object[] additionalParameters = { "boing", new Integer(0) };
                //EventDispatcher.Publish(new EventData(EventActionType.OnStop, EventCategoryType.Sound2D, additionalParameters));
            }
        }
        private void demoCameraChange()
        {
            //only single in single screen layout since cycling in multi-screen is meaningless
            if (this.screenManager.ScreenType == ScreenUtility.ScreenType.SingleScreen && this.keyboardManager.IsFirstKeyPress(Keys.F1))
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnCameraCycle, EventCategoryType.Camera));
            }
        }
        private void demoHideDebugInfo()
        {
            //show/hide debug info
            if (this.keyboardManager.IsFirstKeyPress(Keys.F7))
            {
                EventDispatcher.Publish(new EventData(EventActionType.OnToggleDebug, EventCategoryType.Debug));
            }
        }
        private void demoUIProgressUpdate()
        {
            //testing event generation for UIProgressController
            if (this.keyboardManager.IsFirstKeyPress(Keys.F9))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerOneProgressControllerID, -1 };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.F10))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerOneProgressControllerID, 1 };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }

            if (this.keyboardManager.IsFirstKeyPress(Keys.F11))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerTwoProgressControllerID, -1 };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.F12))
            {
                //increase the left progress controller by 2
                object[] additionalEventParams = { AppData.PlayerTwoProgressControllerID, 3 };
                EventDispatcher.Publish(new EventData(EventActionType.OnHealthDelta, EventCategoryType.Player, additionalEventParams));
            }
        }
        private void demoAlphaChange()
        {
            if (this.drivableBoxObject != null)
            {
                //testing event generation on opacity change - see DrawnActor3D::Alpha setter
                if (this.keyboardManager.IsFirstKeyPress(Keys.F5))
                {
                    this.drivableBoxObject.Alpha -= 0.05f;
                }
                else if (this.keyboardManager.IsFirstKeyPress(Keys.F6))
                {
                    this.drivableBoxObject.Alpha += 0.05f;
                }
            }
        }
        #endregion

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
