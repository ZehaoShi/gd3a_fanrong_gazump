﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDApp
{
    public class MyAppMenuManager : MenuManager
    {
        static bool menuSoundOn = false;
        static double ButtonSoundResetTime;
        public static bool pressMenu;

        public MyAppMenuManager(Game game, MouseManager mouseManager, KeyboardManager keyboardManager, CameraManager cameraManager,
            SpriteBatch spriteBatch, EventDispatcher eventDispatcher,
            StatusType statusType) : base(game, mouseManager, keyboardManager, cameraManager, spriteBatch, eventDispatcher, statusType)
        {

        }

        #region Event Handling
        protected override void EventDispatcher_MenuChanged(EventData eventData)
        {
            //call base method to show/hide the menu
            base.EventDispatcher_MenuChanged(eventData);

            //then generate sound events particular to your game e.g. play background music in a menu
            if (eventData.EventType == EventActionType.OnStart)
            {
                ////add event to stop background menu music here...
                //object[] additionalParameters = { "menu elevator music", 1 };
                //EventDispatcher.Publish(new EventData(EventActionType.OnStop, EventCategoryType.Sound2D, additionalParameters));
            }
            else if (eventData.EventType == EventActionType.OnPause)
            {
                //add event to play background menu music here...
                //object[] additionalParameters = { "menu elevator music" };
                //EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }
        }
        #endregion

        protected override void HandleMouseOver(UIObject currentUIObject, GameTime gameTime)
        {
            //accumulate time over menu item
            //if greater than X milliseconds then play a boing and reset accumulated time
            object[] additionalParameters = { "MenuHover" };
            if (menuSoundOn == false)
            {
                ButtonSoundResetTime = gameTime.TotalGameTime.TotalSeconds;
                menuSoundOn = true;
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
            }
            if (gameTime.TotalGameTime.TotalSeconds - ButtonSoundResetTime > 2 && menuSoundOn == true)
            {
                menuSoundOn = false;
            }
        }



        //add the code here to say how click events are handled by your code
        protected override void HandleMouseClick(UIObject clickedUIObject, GameTime gameTime)
        {
            object[] param;
            //notice that the IDs are the same as the button IDs specified when we created the menu in Main::AddMenuElements()
            switch (clickedUIObject.ID)
            {
                case "startbtn":
                    object[] menuClick = { "TurnStart" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick));
                    DoStart();
                    pressMenu = true;
                    break;

                case "helpbtn":
                    object[] menuClick1 = { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick1));
                    SetActiveList("helpMenu");
                    break;

                case "helpControls":
                    param = new object[]{ "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, param));
                    SetActiveList("helpControls");
                    break;

                case "helpRules":
                    param = new object[] { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, param));
                    SetActiveList("helpRules");
                    break;

                case "exitbtn":
                    object[] menuClick2 = { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick2));
                    DoExit();
                    break;

                case "audiobtn":
                    object[] menuClick3 = { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick3));
                    SetActiveList("audio menu"); //use sceneIDs specified when we created the menu scenes in Main::AddMenuElements()
                    break;

                    case "volumeUpbtn":
                        { //curly brackets scope additionalParameters to be local to this case

                            object[] additionalParameters = { 0.1f , "Default"};
                            EventDispatcher.Publish(new EventData(EventActionType.OnVolumeChange, EventCategoryType.GlobalSound, additionalParameters));
                            object[] menuClick4 = { "hammervolumeselect 2" };
                            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick4));
                        }
                        break;

                    case "volumeDownbtn":
                        {  
                            object[] additionalParameters = { -0.1f , "Default"};
                            EventDispatcher.Publish(new EventData(EventActionType.OnVolumeChange, EventCategoryType.GlobalSound, additionalParameters));
                            object[] menuClick5 = { "hammervolumeselect 2" };
                            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick5));
                        }
                        break;

                    case "volumeMutebtn":
                        {
                            object[] menuClick6 = { "MenuClick"};
                            EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick6));

                            object[] additionalParameters = { 0.0f, "Default"};
                            EventDispatcher.Publish(new EventData(EventActionType.OnMute, EventCategoryType.GlobalSound, additionalParameters));

                    }
                    break;

                case "volumeUnMutebtn":
                    {
                        object[] additionalParameters = { 0.5f, "Default" };
                        EventDispatcher.Publish(new EventData(EventActionType.OnUnMute, EventCategoryType.GlobalSound, additionalParameters));
                        object[] menuClick7 = { "hammervolumeselect 2" };
                        EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick7));
                    }
                    break;

                case "backbtn":
                    object[] menuClick8 = { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick8));
                    SetActiveList("main menu"); //use sceneIDs specified when we created the menu scenes in Main::AddMenuElements()
                    break;

                case "controlsbtn":
                    object[] menuClick9 = { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, menuClick9));
                    SetActiveList("controls menu"); //use sceneIDs specified when we created the menu scenes in Main::AddMenuElements()
                    break;

                default:
                    break;
            }

            //add event to play mouse click sound here...

        }

        private void DoStart()
        {
            //will be received by the menu manager and screen manager and set the menu to be shown and game to be paused
            EventDispatcher.Publish(new EventData(EventActionType.OnStart, EventCategoryType.MainMenu));
        }

        private void DoExit()
        {
            this.Game.Exit();
        }

    }
}