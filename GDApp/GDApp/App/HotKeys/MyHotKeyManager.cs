using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDApp
{
    public class MyHotKeyManager : MenuManager
    {
        Player player;

        public MyHotKeyManager(Game game, MouseManager mouseManager, KeyboardManager keyboardManager, CameraManager cameraManager,
            SpriteBatch spriteBatch, EventDispatcher eventDispatcher,
            StatusType statusType, Player player) : base(game, mouseManager, keyboardManager, cameraManager, spriteBatch, eventDispatcher, statusType)
        {
            this.player = player;
        }

        //public bool visible { get; set; }

        protected override void EventDispatcher_MenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start game event
            if (eventData.EventType == EventActionType.OnStart)
            {
                //turn on update and draw i.e. hide the menu
                this.StatusType = StatusType.Update | StatusType.Drawn;
            }
            //did the event come from the main menu and is it a start game event
            else if (eventData.EventType == EventActionType.OnPause)
            {
                //turn off update and draw i.e. show the menu since the game is paused
                this.StatusType = StatusType.Off;
            }
        }


        protected override void HandleMouseClick(UIObject clickedUIObject, GameTime gameTime)
        {
            //notice that the IDs are the same as the button IDs specified when we created the menu in Main::AddMenuElements()
            switch (clickedUIObject.ID)
            {
                case "houseBtn":
                    object[] menuClick = { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnClick, EventCategoryType.Player, menuClick));
                    player.buildHouse();
                    break;

                case "centreBtn":
                    object[] menuClick2 = { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnClick, EventCategoryType.Player, menuClick2));
                    player.buildCentre();
                    break;

                case "hotelBtn":
                    object[] menuClick3 = { "MenuClick" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnClick, EventCategoryType.Player, menuClick3));
                    
                    player.buildHotel();
                    break;

                default:
                    break;
            }

        }


        //protected override void HandleMouseOver(UIObject currentUIObject, GameTime gameTime)
        //{
        //    switch (currentUIObject.ID) {
        //        case "houseBtn":
        //            EventDispatcher.Publish(new EventData(EventActionType.OnHover, EventCategoryType.Player));
        //            break;

        //        case "centreBtn":
        //            EventDispatcher.Publish(new EventData(EventActionType.OnHover, EventCategoryType.Player));
        //            break;

        //        case "hotelBtn":
        //            EventDispatcher.Publish(new EventData(EventActionType.OnHover, EventCategoryType.Player));
        //            break;

        //        default:
        //            break;
        //    }

        //}


        public void setPlayer(Player player) {
            this.player = player;
        } 

    }
}

