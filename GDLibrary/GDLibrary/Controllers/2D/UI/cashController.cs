﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class cashController : UIController
    {
        private StatusType statusType = StatusType.Off;
        private int i = 1;
        private EventDispatcher eventDispatcher;
        private Player player;

        public cashController(string id, ControllerType controllerType, EventDispatcher eventDispatcher, Player player) : base(id, controllerType)
        {
            this.eventDispatcher = eventDispatcher;
            this.player = player;
            registerEventHandling();
        }

        private void registerEventHandling()
        {
            eventDispatcher.MenuChanged += OnMenuChanged;
        }

        private void OnMenuChanged(EventData eventData)
        {
            //did the event come from the main menu and is it a start game event
            if (eventData.EventType == EventActionType.OnStart)
            {
                //turn on update and draw i.e. hide the menu
                this.statusType = StatusType.Update;
            }
            //did the event come from the main menu and is it a start game event
            else if (eventData.EventType == EventActionType.OnPause)
            {
                //turn off update and draw i.e. show the menu since the game is paused
                this.statusType = StatusType.Off;
            }
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            collectIncome(gameTime, actor);
        }

        private void collectIncome(GameTime gameTime, IActor actor)
        {
            UITextObject textObject = actor as UITextObject;

            textObject.Text = "" + this.player.balance;
        }
    }
}
