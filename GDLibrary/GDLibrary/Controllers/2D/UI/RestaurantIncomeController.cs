﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class RestaurantIncomeController : UIController
    {
        public RestaurantIncomeController(string id, ControllerType controllerType) : base(id, controllerType)
        {
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            UITextObject textObject = actor as UITextObject;

            textObject.Text = "" + ShoppingCenter.TotalIncome;

        }
    }

}
