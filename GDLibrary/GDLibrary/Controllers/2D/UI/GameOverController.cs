﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Xna.Framework;
    using System.Text;

    namespace GDLibrary
    {
        public class GameOverController : UIController
        {
            private StatusType statusType = StatusType.Off;
            private int i = 1;
            private EventDispatcher eventDispatcher;
            private UIButtonObject parent;

            Player player;

        public GameOverController(string id, ControllerType controllerType, EventDispatcher eventDispatcher, UIButtonObject parent, Player player) : base(id, controllerType)
            {
                this.parent = parent;
                this.player = player;
                this.eventDispatcher = eventDispatcher;
                registerEventHandling();
            }

            private void registerEventHandling()
            {
                eventDispatcher.hudChanged += OnHudChanged;
            }

            private void OnHudChanged(EventData eventData)
            {
                parent.StatusType = StatusType.Update | StatusType.Drawn;
            }

        public override void Update(GameTime gameTime, IActor actor)
        {
            parent.Text = player.playerScore.ToString();


        }

    }
    }