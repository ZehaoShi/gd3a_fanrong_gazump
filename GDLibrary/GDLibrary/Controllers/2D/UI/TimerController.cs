﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class TimerController : UIController
    {
        private Player player;
        public TimerController(string id, ControllerType controllerType, Player player) : base(id, controllerType)
        {
            this.player = player;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            UITextObject textObject = actor as UITextObject;

            textObject.Text = player.timeLeftMinutes + " : " + player.timeLeftSeconds60;
        }
        public Player Player
        {
            get
            {
                return player; 
            }
        }
    }
}
