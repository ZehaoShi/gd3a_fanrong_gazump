﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class PopulationController : TimerController
    {
        public PopulationController(string id, ControllerType controllerType, Player player) : base(id, controllerType, player)
        {
        }
        public override void Update(GameTime gameTime, IActor actor)
        {
            UITextObject textObject = actor as UITextObject;

            //if (Player.cityPopulation > Player.cityCapacity)
            //{
            //    textObject.Color = Color.Red;
            //}
            //else {
            //    textObject.Color = Color.Yellow;
            //}

            //int oldTime = Player.timeLeftSeconds60;


            if (Player.cityPopulation > Player.cityCapacity && Player.timeLeftSeconds60 % 2 == 0)
            {
                textObject.Color = Color.Red;
                //oldTime = Player.timeLeftSeconds60;
            }
            else if (Player.cityPopulation > Player.cityCapacity && Player.timeLeftSeconds60 % 2 != 0)
            {
                textObject.Color = Color.Yellow;
            }
            else {
                textObject.Color = Color.Yellow;
            }



            textObject.Text = this.Player.cityPopulation + " / " + this.Player.cityCapacity + " Max";

        }
    }
}
