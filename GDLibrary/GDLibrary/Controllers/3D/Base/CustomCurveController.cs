﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class CustomCurveController : CurveController
    {
        private CameraManager cameraManager;
        private Vector3 oldTranslation;

        public CustomCurveController(string id, ControllerType controllerType, Transform3DCurve transform3DCurve, PlayStatusType playStatusType, CameraManager cameraManager) : base(id, controllerType, transform3DCurve, playStatusType)
        {
            this.cameraManager = cameraManager;

        }
        public override void Update(GameTime gameTime, IActor actor)
        {

            base.Update(gameTime, actor);

            Camera3D camera = actor as Camera3D;
            if (camera.StatusType == StatusType.Update) {
                checkIfLastPoint(camera, gameTime);
            }
        }

        private void checkIfLastPoint(Camera3D camera, GameTime gameTime)
        {
            if (camera.Transform.Translation == oldTranslation)
            {
                this.cameraManager.Remove((cam) => (cam.ID == camera.ID));
                this.cameraManager.SetActiveCamera((cam) => (cam.ID == "player camera"));
                EventDispatcher.Publish(new EventData(EventActionType.OnStart, EventCategoryType.IntroCamera));
            }
            
            this.oldTranslation = camera.Transform.Translation;
        }
    }
}
