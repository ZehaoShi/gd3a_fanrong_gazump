﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class RouteControllerWithLook : RouteController
    {
        Vector3 oldPoint;

        public RouteControllerWithLook(string id, ControllerType controllerType, Vector3[] points, float speed, int startIndex) : base(id, controllerType, points, speed, startIndex)
        {
        }
        public override void Update(GameTime gameTime, IActor actor)
        {
            Actor3D actor3D = actor as Actor3D;
            base.Update(gameTime, actor);

            //
            if (oldPoint != this.currentPoint)
            {
                Vector3 newLook = this.currentPoint - actor3D.Transform.Translation;

                newLook.Normalize();

                // update the rotation based on the look
                float LookX = actor3D.Transform.Look.X;
                float LookY = actor3D.Transform.Look.Y;
                float LookZ = actor3D.Transform.Look.Z;

                float degrees = (float)(Math.Atan2(LookX * newLook.Z - LookZ * newLook.X, LookX * newLook.X + LookZ * newLook.Z) * (180 / Math.PI));

                //System.Diagnostics.Debug.WriteLine(degrees);

                actor3D.Transform.RotateAroundYBy(-degrees);
                // set 
                actor3D.Transform.Look = newLook;
                oldPoint = this.currentPoint;
            }
        }
    }
}
