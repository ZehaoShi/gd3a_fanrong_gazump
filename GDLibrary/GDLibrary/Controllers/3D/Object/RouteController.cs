﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class RouteController : Controller
    {

        Vector3[] points;
        protected Vector3 currentPoint;
        private float speed;

        int currentIndex = 0;

        public RouteController(string id, ControllerType controllerType, Vector3[] points, float speed, int startIndex) : base(id, controllerType)
        {
            this.points = points;
            this.speed = speed;
            this.currentIndex = startIndex;
            this.currentPoint = points[startIndex];
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            updateActorTranslation(gameTime, actor as Actor3D);
            
            base.Update(gameTime, actor);
        }

        private void updateActorTranslation(GameTime gameTime, Actor3D actor)
        {
            float deltaTime = gameTime.ElapsedGameTime.Milliseconds/1000f;
            Vector3 deltaVector = this.currentPoint - actor.Transform.Translation;
            float distanceToNextPoint = deltaVector.Length();
            if (distanceToNextPoint > deltaTime * this.speed)
            {
                // calculate the addition vector by multiplying the distance covered by the direction Vector of the delta
                // add the result to the actor's current translation
                deltaVector.Normalize();
                actor.Transform.Translation +=  deltaVector * deltaTime * this.speed;
            } else
            {
                currentIndex++;

                if (currentIndex < points.Length)
                {
                    currentPoint = points[currentIndex];
                }
                else
                {
                    currentIndex = 0;
                    currentPoint = points[0];
                }
            }
        }
    }
}
