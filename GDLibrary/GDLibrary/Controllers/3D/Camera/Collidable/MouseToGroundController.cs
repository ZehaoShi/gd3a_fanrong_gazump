﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class MouseToGroundController : Controller
    {
        MouseManager mouseManager;
        Camera3D camera;
        public PlayStatusType statusType = PlayStatusType.Play;
        private Vector3 oldObjectPosition;
        private short snapBy = 50;

        public MouseToGroundController(string id, ControllerType controllerType, Camera3D camera, MouseManager mouseManager) : base(id, controllerType)
        {
            this.camera = camera;
            this.mouseManager = mouseManager;
        }
        public override void Update(GameTime gameTime, IActor actor)
        {
                CollidableObject obj = actor as CollidableObject;

                putObjectOnMousePosition(obj);
        }
        public override bool SetControllerPlayStatus(PlayStatusType playStatusType)
        {
            this.statusType = playStatusType;
            return false;
        }

        private void putObjectOnMousePosition(CollidableObject obj)
        {
            //System.Diagnostics.Debug.WriteLine(obj.ID + ": " + obj.Body.Position); 
            if (this.statusType == PlayStatusType.Play)
            {

                //System.Diagnostics.Debug.WriteLine(camera.Projection);

                Vector3 mouseDirection = mouseManager.GetMouseRayDirection(camera);

                // find out the scalar for Y to be 0 (ground Y)
                float scalar = Math.Abs(camera.Transform.Translation.Y / mouseDirection.Y);

                // put object on the ground at direction where mouse is pointing at
                Vector3 newObjectPosition = camera.Transform.Translation + ((scalar) * mouseDirection);

                // snap to grid                           
                float modulusX = newObjectPosition.X % snapBy;
                float modulusZ = newObjectPosition.Z % snapBy;

                newObjectPosition.X = (Math.Abs(modulusX) <= snapBy / 2) ? newObjectPosition.X - modulusX : (modulusX > 0) ? newObjectPosition.X + (snapBy - modulusX) : newObjectPosition.X - (snapBy + modulusX);
                newObjectPosition.Z = (Math.Abs(modulusZ) <= snapBy / 2) ? newObjectPosition.Z - modulusZ : (modulusZ > 0) ? newObjectPosition.Z + (snapBy - modulusZ) : newObjectPosition.Z - (snapBy + modulusZ);

                this.oldObjectPosition = newObjectPosition;

                obj.Body.Position = newObjectPosition;
                obj.Transform.Translation = newObjectPosition;
            }
            else
            {

                obj.Body.Position = oldObjectPosition;
                obj.Transform.Translation = oldObjectPosition;
            }
        }
    }
}
