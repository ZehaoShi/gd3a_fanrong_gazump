﻿/*
Function: 		A third person controller to a camera (i.e. the parent actor) which causes the camera to track a target actor in 3rd person mode.
Author: 		NMCG
Version:		1.0
Date Updated:	24/10/17
Bugs:			None
Fixes:			None
*/
using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    public class ThirdPersonController : TargetController
    {
        #region Fields
        private float elevationAngle, distance, scrollSpeedDistanceMultiplier, scrollSpeedElevationMultiplier;

        //used to dampen camera movement
        private Vector3 oldTranslation;
        private Vector3 oldCameraToTarget;
        private float translationLerpSpeed, lookLerpSpeed;
        private float cameraSpeed;
        private MouseManager mouseManager;
        private SoundManager soundManager;
        private KeyboardManager keyboardManager;
        #endregion

        #region Properties
        public float TranslationLerpSpeed
        {
            get
            {
                return translationLerpSpeed;
            }
            set
            {

                //lerp speed should be in the range >0 and <=1
                translationLerpSpeed = (value > 0) && (value <= 1) ? value : 0.1f;
            }
        }
        public float LookLerpSpeed
        {
            get
            {
                return lookLerpSpeed;
            }
            set
            {

                //lerp speed should be in the range >0 and <=1
                lookLerpSpeed = (value > 0) && (value <= 1) ? value : 0.1f;
            }
        }
        public float ScrollSpeedDistanceMultiplier
        {
            get
            {
                return scrollSpeedDistanceMultiplier;
            }
            set
            {
                //distanceScrollMultiplier should not be lower than 0
                scrollSpeedDistanceMultiplier = (value > 0) ? value : 1;
            }
        }

        public float ScrollSpeedElevationMultiplier
        {
            get
            {
                return scrollSpeedElevationMultiplier;
            }
            set
            {
                //scrollSpeedElevationMulitplier should not be lower than 0
                scrollSpeedElevationMultiplier = (value > 0) ? value : 1;
            }
        }

        public float Distance
        {
            get
            {
                return distance;
            }
            set
            {
                //distance should not be lower than 0
                distance = (value > 0) ? value : 1;
            }
        }
        public float ElevationAngle
        {
            get
            {
                return elevationAngle;
            }
            set
            {
                elevationAngle = value % 360;
                elevationAngle = MathHelper.ToRadians(elevationAngle);
            }
        }
        #endregion

        public ThirdPersonController(string id, ControllerType controllerType, Actor targetActor,
            float distance, float scrollSpeedDistanceMultiplier, float elevationAngle, 
                float scrollSpeedElevationMultiplier, float translationLerpSpeed, float lookLerpSpeed, MouseManager mouseManager, KeyboardManager keyboardManager, SoundManager soundManager)
            : base(id, controllerType, targetActor)
        {
            //call properties to set validation on distance and radian conversion
            this.Distance = distance;

            //allows us to control distance and elevation from the mouse scroll wheel
            this.ScrollSpeedDistanceMultiplier = scrollSpeedDistanceMultiplier;
            this.ScrollSpeedElevationMultiplier = scrollSpeedElevationMultiplier;
            //notice that we pass the incoming angle through the property to convert it to radians
            this.ElevationAngle = elevationAngle;

            //dampen camera translation reaction
            this.translationLerpSpeed = translationLerpSpeed;
            //dampen camera rotation reaction
            this.lookLerpSpeed = lookLerpSpeed;

            // cameraSpeed
            this.cameraSpeed = 0.5f;

            //used to change elevation angle or distance from target - See UpdateFromScrollWheel()
            this.mouseManager = mouseManager;
            this.soundManager = soundManager;
            this.keyboardManager = keyboardManager;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            UpdateFromScrollWheel(gameTime, actor as Actor3D, this.TargetActor as Actor3D);
            MovementCheck(gameTime, actor as Camera3D);
            UpdateSoundManager(actor as Actor3D);
            LimitPosition();
            UpdateParent(gameTime, actor as Actor3D, this.TargetActor as Actor3D);
        }

        private void UpdateSoundManager(Actor3D actor3D)
        {
            this.soundManager.UpdateListenerPosition(actor3D.Transform);
        }

        private void LimitPosition()
        {
            PlayerCameraTarget target = this.TargetActor as PlayerCameraTarget;

            if (target.Transform.Translation.X > 700)
            {
                target.Transform.Translation = new Vector3(700, target.Transform.Translation.Y, target.Transform.Translation.Z);
            }
            if (target.Transform.Translation.X < -350)
            {
                target.Transform.Translation = new Vector3(-350, target.Transform.Translation.Y, target.Transform.Translation.Z);
            }
            if (target.Transform.Translation.Z > 350)
            {
                target.Transform.Translation = new Vector3(target.Transform.Translation.X, target.Transform.Translation.Y, 350);
            }
            if (target.Transform.Translation.Z < -350)
            {
                target.Transform.Translation = new Vector3(target.Transform.Translation.X, target.Transform.Translation.Y, -350);
            }
        }

        private void MovementCheck(GameTime gameTime, Camera3D camera)
        {
            //System.Diagnostics.Debug.WriteLine(this.mouseManager.Position);

            PlayerCameraTarget target = this.TargetActor as PlayerCameraTarget;

            bool isLeft = this.mouseManager.Position.X <= 0;
            bool isRight = this.mouseManager.Position.X >= camera.Viewport.Width;
            bool isTop = this.mouseManager.Position.Y <= 0;
            bool isBottom = this.mouseManager.Position.Y >= camera.Viewport.Height;
            bool isTopLeft = isLeft && isTop;
            bool isBottomLeft = isLeft && isBottom;
            bool isTopRight = isRight && isTop;
            bool isBottomRight = isRight && isBottom;

            float moveAmount = this.cameraSpeed * gameTime.ElapsedGameTime.Milliseconds;

            if (isTopLeft)
            {
                // move north west
                target.Transform.Translation -= -target.Transform.Right * moveAmount + target.Transform.Look * moveAmount;
            }
            else if (isBottomLeft)
            {
                // move South west
                target.Transform.Translation -= -target.Transform.Right * moveAmount - target.Transform.Look * moveAmount;
            }
            else if (isBottomRight)
            {
                // move south east
                target.Transform.Translation -= target.Transform.Right * moveAmount - target.Transform.Look * moveAmount;
            }
            else if (isTopRight)
            {
                // move north east
                target.Transform.Translation -= target.Transform.Right * moveAmount + target.Transform.Look * moveAmount;
            }
            else if (isLeft)
            {
                // move left
                target.Transform.Translation -= -target.Transform.Right * moveAmount;
            }
            else if (isRight)
            {
                // move right
                target.Transform.Translation -= target.Transform.Right * moveAmount;
            }
            else if (isBottom)
            {
                // move back
                target.Transform.Translation -= -target.Transform.Look * moveAmount;
            }
            else if (isTop)
            {
                target.Transform.Translation -= target.Transform.Look * moveAmount;
            }

            // rotation
            if (keyboardManager.IsKeyDown(Keys.A))
            {
                target.Transform.RotateAroundYBy(-moveAmount / 5);
            }
            else if (keyboardManager.IsKeyDown(Keys.D))
            {
                target.Transform.RotateAroundYBy(moveAmount / 5);
            }

            //System.Diagnostics.Debug.WriteLine(target.Transform.Translation);
        }

        private void UpdateParent(GameTime gameTime, Actor3D parentActor, Actor3D targetActor)
        {
            if (targetActor != null)
            {
                //rotate the target look around the target right to get a vector pointing away from the target at a specified elevation
                Vector3 cameraToTarget = Vector3.Transform(targetActor.Transform.Look,
                    Matrix.CreateFromAxisAngle(targetActor.Transform.Right, this.elevationAngle));

                //normalize to give unit length, otherwise distance from camera to target will vary over time
                cameraToTarget.Normalize();

                //set the position of the camera to be a set distance from target and at certain elevation angle
                parentActor.Transform.Translation = Vector3.Lerp(this.oldTranslation, cameraToTarget * this.distance + targetActor.Transform.Translation, this.translationLerpSpeed);
                //set the camera to look at the target object
                parentActor.Transform.Look = Vector3.Lerp(this.oldCameraToTarget, cameraToTarget, this.lookLerpSpeed);

                //store old values for lerp
                this.oldTranslation = parentActor.Transform.Translation;
                this.oldCameraToTarget = -cameraToTarget;
            }
        }

        private void UpdateFromScrollWheel(GameTime gameTime, Actor3D parentActor, Actor3D targetActor)
        {
            //get magnitude and direction of scroll change
            float scrollWheelDelta = -this.mouseManager.GetDeltaFromScrollWheel() * gameTime.ElapsedGameTime.Milliseconds;

            //if something changed then update
            if (scrollWheelDelta != 0)
            {
                //move camera closer to, or further, from the target
                if (/*parentActor.Transform.Translation.Y < 500*/ (this.Distance + this.scrollSpeedDistanceMultiplier * scrollWheelDelta) <= 500 && (this.Distance + this.scrollSpeedDistanceMultiplier * scrollWheelDelta) >= 100) // limit the camera movement
                {
                    this.Distance += this.scrollSpeedDistanceMultiplier * scrollWheelDelta;
                }
            }
        }

        public override bool Equals(object obj)
        {
            ThirdPersonController other = obj as ThirdPersonController;

            if (other == null)
                return false;
            else if (this == other)
                return true;

            return this.elevationAngle.Equals(other.ElevationAngle)
                    && this.distance.Equals(other.Distance)
                      && this.translationLerpSpeed.Equals(other.TranslationLerpSpeed)
                        && this.lookLerpSpeed.Equals(other.LookLerpSpeed)
                        && base.Equals(obj);
        }

        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 31 + this.elevationAngle.GetHashCode();
            hash = hash * 17 + this.distance.GetHashCode();
            hash = hash * 41 + this.translationLerpSpeed.GetHashCode();
            hash = hash * 47 + this.lookLerpSpeed.GetHashCode();
            hash = hash * 11 + base.GetHashCode();
            return hash;
        }

        //be careful when cloning this controller as we will need to reset the target actor - assuming the clone attaches to a different target
        public override object Clone()
        {
            return new ThirdPersonController("clone - " + this.ID, //deep
                this.ControllerType, //deep
                this.TargetActor as Actor, //shallow - a ref
                this.distance, //deep
                this.scrollSpeedDistanceMultiplier, //deep
                this.elevationAngle, //deep
                this.scrollSpeedElevationMultiplier, //deep
                this.translationLerpSpeed, //deep
                this.lookLerpSpeed, //deep
                this.mouseManager,
                this.keyboardManager,
                this.soundManager); //shallow - a ref
        }
    }
}