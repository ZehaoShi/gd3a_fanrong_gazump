﻿using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class Sound2D
    {
        Cue sound;
        SoundManager soundManager;

        public Sound2D(string cueName, SoundManager soundManager, EventDispatcher eventDispatcher)
        {
            eventDispatcher.MenuChanged += menuChanged;
            this.soundManager = soundManager;
            sound = soundManager.soundBank.GetCue(cueName);
            sound.Play();
            sound.Pause();
        }

        public void menuChanged(EventData eventData)
        {
            if (eventData.EventCategoryType == EventCategoryType.MainMenu && eventData.EventType == EventActionType.OnStart)
            {
                sound.Resume();
            }
            else if (eventData.EventCategoryType == EventCategoryType.MainMenu && eventData.EventType == EventActionType.OnPause)
            {
                sound.Pause();
            }
        }
    }
}
