﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class Sound3D : Actor3D
    {
        public Cue3D sound = new Cue3D();
        public SoundManager soundManager;

        public Sound3D(string id, ActorType actorType, Transform3D transform, StatusType statusType, SoundManager soundManager, string CueName, EventDispatcher eventDispatcher) : base(id, actorType, transform, statusType)
        {
            this.sound.Emitter = new AudioEmitter();
            this.sound.Cue = soundManager.soundBank.GetCue(CueName);

            eventDispatcher.MenuChanged += EventDispatcher_MenuChanged;

            this.soundManager = soundManager;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (this.StatusType == StatusType.Update) {
                updateEmitterPosition();
            } else
            {
                this.soundManager.PauseCue3D(sound);
            }
        }

        protected virtual void EventDispatcher_MenuChanged(EventData eventData)
        {
            // resume
            if (eventData.EventType == EventActionType.OnStart && eventData.EventCategoryType == EventCategoryType.MainMenu){
                this.soundManager.ResumeCue3D(sound);
                this.StatusType = StatusType.Update;

            } 
            // paused
            else if (eventData.EventType == EventActionType.OnPause && eventData.EventCategoryType == EventCategoryType.MainMenu)
            {
                this.StatusType = StatusType.Off;
                this.soundManager.PauseCue3D(sound);
            }
        }

        private void updateEmitterPosition()
        {
            sound.Emitter.Position = this.Transform.Translation;
        }

        public void play()
        {
            this.soundManager.playCue3D(this.sound);
        }
    }
}
