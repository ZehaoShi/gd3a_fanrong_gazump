﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    public class Player : GameComponent, IGameComponent
    {
        public StatusType statusType = StatusType.Off;
        private bool introFinished = false;
        private string name = "";
        public int cityPopulation = 0;
        public int cityCapacity = 0;
        public long balance = 0;
        public int populationIncreaseRate = 0;
        private int tempIncreaseRate = 0;
        private int populationIncreaseInterval = 10; // seconds
        private short penaltyRate = 15;
        private int tempPenaltyRate = 0;
        private short penaltyInterval = 10;
        private KeyboardManager keyboardManager = null;
        private MouseManager mouseManager = null;
        private ObjectManager objectManager;


        

        private ContentDictionary<EffectParameters> effectDictionary;
        private ContentDictionary<Model> modelDictionary;
        private ContentDictionary<Texture2D> textureDictionary;
        private Camera3D camera;
        private EventDispatcher eventDispatcher;
        public Building buildingAboutToBeBuilt = null;
        private EffectParameters effectParameters;
        GraphicsDeviceManager graphics;
        private int penaltyStartTime;
        public static int timerStartMilliseconds = 301000; 
        private int timeLeftMilliseconds;
        private int totalGameplayTime = 0;
        public static int TotalTime = 0;
        public int timeLeftSeconds60;
        public int timeLeftMinutes;
        public static bool gameFinished = false;
        private bool isCounterTimeSoundOn = false;
        static double IllegalPopulation = 10;
        static float IllegalPopulationRate = 0.1f;
        private int i = 4;
        private int IEpop = 1;
        private int cityRating = 0;
        public /*static*/ double playerScore = 0;
        public int startTimeScore;
        public static int citizen = 0;
        private Building oldBuildingToBeBuilt;


        #region Properties

        public int CityRating
        {
            get
            {
                return this.cityRating; 
            }
        }
        public void reduceCityRating(int value)
        {
            if (this.cityRating - value >= 0)
            {
                this.cityRating -= value;
            } else
            {
                this.cityRating = 0;
            }
        }
        public void increaseCityRating(int value)
        {
                this.cityRating += value;
        }



        #endregion 

        public Player(Game game, string name, long startingBalance, int startingCityPopulation, int startingCityCapacity, KeyboardManager keyboardManager, MouseManager mouseManager, ObjectManager objectManager, EffectParameters effectParameters, ContentDictionary<Model> modelDictionary, ContentDictionary<Texture2D> textureDictionary, Camera3D camera, EventDispatcher eventDispatcher) : base(game)
        {
            this.name = name;
            this.balance = startingBalance;
            this.cityPopulation = startingCityPopulation;
            this.cityCapacity = startingCityCapacity;
            this.mouseManager = mouseManager;
            this.objectManager = objectManager;
            this.keyboardManager = keyboardManager;
            this.effectParameters = effectParameters;
            this.modelDictionary = modelDictionary;
            this.textureDictionary = textureDictionary;
            this.camera = camera;
            this.eventDispatcher = eventDispatcher;




           




            eventDispatcher.introFinished += startGame;
            eventDispatcher.MenuChanged += OnMenuChanged;

            this.statusType = StatusType.Off;

            this.timeLeftMilliseconds = timerStartMilliseconds;
            startTimeScore = timerStartMilliseconds;
        }
        public void OnMenuChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnPause && eventData.EventCategoryType == EventCategoryType.MainMenu && this.introFinished == true)
            {
                this.statusType = StatusType.Off;
            }
            else if (eventData.EventType == EventActionType.OnStart && eventData.EventCategoryType == EventCategoryType.MainMenu  && this.introFinished == true)
            {
                this.statusType = StatusType.Update;
            }
        }

        private void startGame(EventData eventData)
        {
            this.statusType = StatusType.Update;
            this.introFinished = true;
        }

        public /*static */bool GameFinished
        {
            get
            {
                return gameFinished;
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (this.statusType == StatusType.Update) {
                // update stats
                updatePopulationAndBalance(gameTime);
                updateTimer(gameTime);
                updateCounterSound(gameTime);
                updateIllegalPopulation(gameTime);
                collectIncome(gameTime);
                // check for inputs
                checkKeyboardInput();
                checkMouseInput();
                //update scoreboard
                updateScoreboard();
            }
        }

        private void updateScoreboard()
        {
            if (GameFinished == true)
            {
                playerScore = Math.Round((ShoppingCenter.totalIncome + Hotel.totalIncome) * 3.0 + this.balance * 4.0 + (this.cityCapacity - this.cityPopulation) * 3 + (startTimeScore - timeLeftMilliseconds) * 0.02);
                System.Diagnostics.Debug.WriteLine(playerScore);
            }
        }

        private void collectIncome(GameTime gameTime)
        {
            if (timeLeftSeconds60 == 45 && this.statusType == StatusType.Update && Player.gameFinished != true && i == 1)
            {
                i = 4;
                this.balance += ShoppingCenter.TotalIncome;
                this.balance += Hotel.TotalIncome;
                if (ShoppingCenter.TotalIncome != 0 || Hotel.TotalIncome != 0)
                {
                    object[] additionalParameters = { "CollectIncome" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                }
            }

            if (timeLeftSeconds60 == 30 && Player.gameFinished != true && this.statusType == StatusType.Update && i == 4)
            {
                i--;
                this.balance += ShoppingCenter.TotalIncome;
                this.balance += Hotel.TotalIncome;
                if (ShoppingCenter.TotalIncome != 0 || Hotel.TotalIncome != 0)
                {
                    object[] additionalParameters = { "CollectIncome" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                }
            }
            if (timeLeftSeconds60 == 15 && Player.gameFinished != true && this.statusType == StatusType.Update && i == 3)
            {
                i--;
                this.balance += ShoppingCenter.TotalIncome;
                this.balance += Hotel.TotalIncome;
                if (ShoppingCenter.TotalIncome != 0 || Hotel.TotalIncome != 0)
                {
                    object[] additionalParameters = { "CollectIncome" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                }
            }
            if(timeLeftSeconds60 == 0 && Player.gameFinished != true && this.statusType == StatusType.Update && i == 2)
            {
                i--;
                this.balance += ShoppingCenter.TotalIncome;
                this.balance += Hotel.TotalIncome;
                if (ShoppingCenter.TotalIncome != 0 || Hotel.TotalIncome != 0)
                {
                    object[] additionalParameters = { "CollectIncome" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                }
            }
        }

        private void updateIllegalPopulation(GameTime gameTime)
        {
            if (IEpop == 1 && timeLeftSeconds60 == 10 && this.statusType == StatusType.Update && Player.gameFinished != true)
            {
                IEpop++;
                IllegalPopulationRate += 0.5f;
                this.cityPopulation += (int)(IllegalPopulation * IllegalPopulationRate);
                System.Diagnostics.Debug.WriteLine("Illegal Population: " + (int)(IllegalPopulation * IllegalPopulationRate));
            }
            if (IEpop == 2 && timeLeftSeconds60 == 50 && this.statusType == StatusType.Update && Player.gameFinished != true)
            {
                IEpop++;
                IllegalPopulationRate += 0.5f;
                this.cityPopulation += (int)(IllegalPopulation * IllegalPopulationRate);
                System.Diagnostics.Debug.WriteLine((int)(IllegalPopulation * IllegalPopulationRate));
            }
            if (IEpop == 3 && timeLeftSeconds60 == 30 && this.statusType == StatusType.Update && Player.gameFinished != true)
            {
                IllegalPopulationRate += 0.5f;
                this.cityPopulation += (int)(IllegalPopulation * IllegalPopulationRate);
                System.Diagnostics.Debug.WriteLine((int)(IllegalPopulation * IllegalPopulationRate));
                IEpop = 1;
            }
        }

        private void updateCounterSound(GameTime gameTime)
        {
            if (timeLeftSeconds60 <  20 && timeLeftMinutes  == 0 && isCounterTimeSoundOn != true && gameFinished == false)
            {
                object[] additionalParameters = { "AlarmWarning" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                isCounterTimeSoundOn = true;
            }
        }

        private void updateTimer(GameTime gameTime)
        {
            this.timeLeftMilliseconds -= (int)gameTime.ElapsedGameTime.Milliseconds;
            this.totalGameplayTime += (int)gameTime.ElapsedGameTime.Milliseconds;
            TotalTime += (int)gameTime.ElapsedGameTime.Milliseconds;
            timeLeftSeconds60 = (this.timeLeftMilliseconds / 1000) - (((this.timeLeftMilliseconds / 1000) / 60) * 60);

            timeLeftMinutes = ((this.timeLeftMilliseconds / 1000) / 60);

            // check if time limit reached
            if (timeLeftMilliseconds <= 0)
            {
                if(gameFinished == false)
                {
                    System.Diagnostics.Debug.WriteLine("Game has Ended!!");
                    // set status to be off
                    gameFinished = true;
                    // publish event to sound to play game end
                    object[] additionalParameters2 = { "GameWin" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters2));
                    // publish event to show the game over screen
                    EventDispatcher.Publish(new EventData(EventActionType.OnGameOver, EventCategoryType.HUD));
                    // publish event to Game end UI Elements and to In Game UI to hide
                }
                this.statusType = StatusType.Off;
            }
        }

        private void checkMouseInput()
        {
            if (buildingAboutToBeBuilt != null)
            {
                if (mouseManager.IsLeftButtonClickedOnce() && oldBuildingToBeBuilt != null)
                {
                    bool success = buildingAboutToBeBuilt.Build();
                    if (success)
                    {
                        Building test = (Building)buildingAboutToBeBuilt.Clone();
                        // publish event to run nearby building status checks
                        EventDispatcher.Publish(new EventData(test, EventActionType.OnNewBuilding, EventCategoryType.Map));
                        buildingAboutToBeBuilt = null;
                    }
                }
                oldBuildingToBeBuilt = buildingAboutToBeBuilt;
            }

            if (buildingAboutToBeBuilt == null) {
                
            }
        }

        private void checkKeyboardInput()
        {
            

            // check if hotkey 1 is pressed (House)
            if (keyboardManager.IsFirstKeyPress(Keys.D1))
            {
                buildHouse();
            }
            else if (keyboardManager.IsFirstKeyPress(Keys.D3))
            {
                buildHotel();
            }
            else if (keyboardManager.IsFirstKeyPress(Keys.D2))
            {
                buildCentre();
            }
        }


        public void buildHouse() {
            if (buildingAboutToBeBuilt == null)
            {

                this.effectParameters.Texture = textureDictionary["house-Texture"];

                Transform3D transform = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 90, 0), 0.4f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

                this.buildingAboutToBeBuilt = new House("house", ActorType.CollidableArchitecture, transform, (EffectParameters)effectParameters.Clone(), modelDictionary["house"], this, this.eventDispatcher);

                this.buildingAboutToBeBuilt.AttachController(new MouseToGroundController("mouse to ground", ControllerType.Track, camera, mouseManager));

                this.buildingAboutToBeBuilt.Enable(false, 1);        

                this.objectManager.Add(buildingAboutToBeBuilt);
            }
            else
            {
                // cancel

                this.objectManager.Remove(buildingAboutToBeBuilt);
                buildingAboutToBeBuilt.Dispose(eventDispatcher);
                buildingAboutToBeBuilt = null;
            }
        }

        public void buildHotel() {
            if (buildingAboutToBeBuilt == null)
            {
                this.effectParameters.Texture = this.textureDictionary["RealHotelTexture"];

                Transform3D transform = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 90, 0), 0.05f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

                this.buildingAboutToBeBuilt = new ShoppingCenter("shopping center", ActorType.CollidableArchitecture, transform, (EffectParameters)effectParameters.Clone(), modelDictionary["shoppingCenter"], this, this.eventDispatcher);

                this.buildingAboutToBeBuilt.AttachController(new MouseToGroundController("mouse to ground", ControllerType.Track, camera, mouseManager));

                this.buildingAboutToBeBuilt.Enable(false, 1);

                this.objectManager.Add(buildingAboutToBeBuilt);
            }
            else
            {
                // cancel

                this.objectManager.Remove(buildingAboutToBeBuilt);
                buildingAboutToBeBuilt.Dispose(eventDispatcher);
                buildingAboutToBeBuilt = null;
            }

        }

        public void buildCentre() {
            if (buildingAboutToBeBuilt == null)
            {
                this.effectParameters.Texture = this.textureDictionary["hotelTexture"];

                Transform3D transform = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 90, 0), 1.5f * Vector3.One, Vector3.UnitX, Vector3.UnitY);

                this.buildingAboutToBeBuilt = new Hotel("hotel", ActorType.CollidableArchitecture, transform, (EffectParameters)effectParameters.Clone(), modelDictionary["hotel"], this, this.eventDispatcher);

                this.buildingAboutToBeBuilt.AttachController(new MouseToGroundController("mouse to ground", ControllerType.Track, camera, mouseManager));

                this.buildingAboutToBeBuilt.Enable(false, 1);

                this.objectManager.Add(buildingAboutToBeBuilt);
            }

            else
            {
                // cancel

                this.objectManager.Remove(buildingAboutToBeBuilt);
                buildingAboutToBeBuilt.Dispose(eventDispatcher);
                buildingAboutToBeBuilt = null;
            }
        }


        private void updatePopulationAndBalance(GameTime gameTime)
        {
            // every x seconds increase the population
            if ((this.totalGameplayTime / 1000) % populationIncreaseInterval == 0 && (this.totalGameplayTime / 1000) > 1)
            {
                this.cityPopulation += tempIncreaseRate;
                this.tempIncreaseRate = 0;
            }
            else
            {
                this.tempIncreaseRate = populationIncreaseRate;
            }

            // check if population is over the capacity
            int homelessPopulation = (int)(this.cityPopulation - this.cityCapacity);

            if (homelessPopulation > 0)
            {
                if (penaltyStartTime == 0)
                {
                    penaltyStartTime = ((this.totalGameplayTime / 1000) == 0) ? 1 : (this.totalGameplayTime / 1000);
                }

                int offset = (this.totalGameplayTime / 1000) - penaltyStartTime;

                // reduce balance every x seconds
                if (offset % penaltyInterval == 0)
                {
                    this.balance -= (long)(this.tempPenaltyRate * homelessPopulation);
                    this.tempPenaltyRate = 0;
                }
                else
                {
                    this.tempPenaltyRate = penaltyRate;
                }
            }
            else
            {
                penaltyStartTime = 0;
            }

            // check if balance is less than 0

            if (balance < 0)
            {
                if(gameFinished == false)
                {
                    System.Diagnostics.Debug.WriteLine("Game Over, No Money Left!!");
                    // set status to be off
                    object[] additionalParameters = { "GameLose" };
                    EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                    object[] additionalParameters2 = { "AlarmWarning"};
                    EventDispatcher.Publish(new EventData(EventActionType.OnPause, EventCategoryType.Sound2D, additionalParameters2));
                    gameFinished = true;
                    EventDispatcher.Publish(new EventData(EventActionType.OnGameOver, EventCategoryType.HUD));
                    
                }
                this.statusType = StatusType.Off;
            }
        }





    }
}
