﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class PlayerCameraTarget : Actor3D
    {
        public PlayerCameraTarget(string id, ActorType actorType, Transform3D transform, StatusType statusType) : base(id, actorType, transform, statusType)
        {

        }
    }
}
