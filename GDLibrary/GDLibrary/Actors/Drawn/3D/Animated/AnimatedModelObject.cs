﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SkinnedModel;

namespace GDLibrary
{
    public class AnimatedModelObject : ModelObject
    {
        public AnimationPlayer animationPlayer;
        public SkinningData skinningData;
        private string takeName;


        public AnimatedModelObject(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters, Model model, string takeName) : base(id, actorType, transform, effectParameters, model)
        {
            this.Model = model;
            skinningData = model.Tag as SkinningData;

            this.takeName = takeName;

            if (skinningData == null)
                throw new InvalidOperationException("The model does not contain a SkinningData tag.");

            animationPlayer = new AnimationPlayer(skinningData);
             
            animationPlayer.StartClip(skinningData.AnimationClips[takeName]);
        }
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            animationPlayer.Update(gameTime.ElapsedGameTime, true, Matrix.Identity);
        }

        public new object Clone()
        {
            return new AnimatedModelObject(this.ID, this.ActorType, this.Transform.Clone() as Transform3D, this.EffectParameters, this.Model, this.takeName);
        }
        public void setID(string ID)
        {
            this.ID = ID;
        }
    }
}
