﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class WorldCursor : CollidableObject
    {
        public WorldCursor(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters, Model model, MaterialProperties mat) : base(id, actorType, transform, effectParameters, model)
        {
            this.EffectParameters.Alpha = 0;
            this.Collision.AddPrimitive(new Box(Vector3.Zero, Matrix.Identity, new Vector3(1, 1, 1)), mat);
            this.Collision.callbackFn += OnCollisionEvent;
            this.Body.ApplyGravity = false;
        }

        private bool OnCollisionEvent(CollisionSkin skin0, CollisionSkin skin1)
        {
            
            return false;
        }
    }
}
