﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class Building : CollidableObject
    {
        public AudioEmitter emitter = new AudioEmitter();

        bool disposed = false;

        public int nearbyHotels = 0;
        public int nearbyShoppingCenters = 0;
        public int nearbyHouses = 0;

        public int price = 0;
        protected bool isBuilt = false;
        protected bool isValidPlaceToBuild = true;
        public Player owner;

        public Building(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters, Model model, Player owner, EventDispatcher eventDispatcher) : base(id, actorType, transform, effectParameters, model)
        {
            this.owner = owner;
            this.Collision.callbackFn += OnCollisionEvent;
            // register for event handling if new building has been placed, passing both location of newly built place and event data

            eventDispatcher.MapChanged += EventDispatcher_MapChanged;
        }
        public Building(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters, Model model, Player owner) : base(id, actorType, transform, effectParameters, model)
        {
            this.owner = owner;
        }

        public void EventDispatcher_MapChanged(EventData eventData)
        {
            // check if building if building is near with this building
            Building building = eventData.Sender as Building;
            float distance = Vector3.Distance(building.Transform.Translation, this.Transform.Translation);
            int distanceLimit = 200;

            if (distance <= distanceLimit && /*Not self*/ distance > 0)
            {
                UpdateBuildingStatus(eventData.EventType, building);
            }
        }

        public void UpdateBuildingStatus(EventActionType actionType, Building building)
        {
            if (actionType == EventActionType.OnNewBuilding)
            {
                this.AddStatusFrom(building);
            } else if (actionType == EventActionType.OnRemoveBuilding) 
            {
                this.RemoveStatusOf(building);
            }
        }

        public virtual void RemoveStatusOf(Building building)
        {
            // override in child classes for specific effect checking
        }

        public virtual void AddStatusFrom(Building newlyBuilt)
        {
            // override in child classes for specific effect checking
        }

        private bool OnCollisionEvent(CollisionSkin skin0, CollisionSkin skin1)
        {
            Actor3D actor = skin1.Owner.ExternalData as Actor3D;

            if (actor.ActorType == ActorType.CollidableArchitecture)
            {
                this.EffectParameters.DiffuseColor = Color.Red;
                isValidPlaceToBuild = false;
            }
            else if (actor.ActorType == ActorType.CollidableGround)
            {

            }
            else if (actor.ActorType == ActorType.Player)
            {

            }


            return false;
        }

        public override void Update(GameTime gameTime)
        {
            if (!isBuilt) {
                this.EffectParameters.DiffuseColor = Color.LightGreen;
                isValidPlaceToBuild = true;
            }
            base.Update(gameTime);
        }
        public virtual bool Build()
        {
            if (isValidPlaceToBuild && (this.owner.balance - this.price) >= 0) {
                // pay money
                this.owner.balance -= this.price;

                // remove the positioning controller and establish on the valid given position
                this.ControllerList.Remove(this.ControllerList.Find((cont) => (cont.GetID() == "mouse to ground")));

                // set Immovable
                this.Enable(true, 1);

                this.Collision.callbackFn -= OnCollisionEvent;

                isBuilt = true;

                // set color back to white
                this.EffectParameters.DiffuseColor = Color.White;

                return true;
            } else
            {
                object[] additionalParameters = { "NegativeBeep" };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound2D, additionalParameters));
                return false;
            }
        }
        public bool IsValidPlaceToBuild
        {
            get
            {
                return this.isValidPlaceToBuild; 
            }
        }
        public new object Clone()
        {
            return new Building(this.ID, ActorType.CollidableArchitecture, this.Transform, this.EffectParameters, this.Model, this.owner);
        }

        public void Dispose(EventDispatcher eventDispatcher)
        {
            eventDispatcher.MapChanged -= EventDispatcher_MapChanged;
            this.disposed = true;
        }
    }
}
