﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class Hotel : Building
    {
        public static int totalIncome = 0;
        public static int num = 0;

        public Hotel(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters, Model model, Player owner, EventDispatcher eventDispatcher) : base(id, actorType, transform, effectParameters, model, owner, eventDispatcher)
        {
            if (Player.TotalTime < Player.timerStartMilliseconds / 2)
            {
                this.price = 500;
            }
            else if (Player.TotalTime >= Player.timerStartMilliseconds / 2)
            {
                this.price = 1000;
            }
            this.Collision.AddPrimitive(new Box(this.Transform.Translation, Matrix.Identity, /*important do not change - cm to inch*/50f * this.Transform.Scale), new MaterialProperties());
        }

        public new object Clone(EventDispatcher eventDispatcher)
        {
            return new House("clone - " + ID, //deep
                this.ActorType,   //deep
                (Transform3D)this.Transform.Clone(),  //deep
                this.EffectParameters.GetDeepCopy(), //hybrid - shallow (texture and effect) and deep (all other fields) 
                this.Model,
                this.owner,
                eventDispatcher); //shallow i.e. a reference
        }

        public static int TotalIncome
        {
            get
            {
                return totalIncome;
            }
        }

        public static int Num
        {
            get
            {
                return num;
            }
        }

        public override bool Build()
        {
            if (this.IsValidPlaceToBuild && (this.owner.balance - this.price) >= 0)
            {
                // do something for hotel

                // sound
                emitter.Position = this.Transform.Translation;
                object[] additionalParameters = { "Build Hotel" , emitter };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound3D, additionalParameters));

                if (Player.TotalTime < Player.timerStartMilliseconds / 2)
                {
                    this.owner.cityCapacity += 40;
                    Hotel.totalIncome += 400;
                    this.owner.populationIncreaseRate += 10;
                }
                else if (Player.TotalTime >= Player.timerStartMilliseconds / 2)
                {
                    this.owner.cityCapacity += 80;
                    Hotel.totalIncome += 600;
                    this.owner.populationIncreaseRate += 25;
                }
                
            }

            return base.Build();
        }

        public override void AddStatusFrom(Building newlyBuilt)
        {
            if (newlyBuilt.ID == "house")
            {
                // apply house effects
                //System.Diagnostics.Debug.WriteLine("theres a house near me");
                //Hotel.totalIncome -= 50;
            }
            else if (newlyBuilt.ID == "shopping center")
            {
                // apply effects to shopping center
               Hotel.totalIncome += 200;
            }
            else if (newlyBuilt.ID == "hotel")
            {
                // apply restaurant effects
            }
            // add more checks for more different building types
        }
    }
}
