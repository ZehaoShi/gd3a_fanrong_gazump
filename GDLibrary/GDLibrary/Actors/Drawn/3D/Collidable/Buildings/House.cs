﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class House : Building
    {
        short numberOfResidents = 5;
        

        public House(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters, Model model, Player owner, EventDispatcher eventDispatcher) : base(id, actorType, transform, effectParameters, model, owner, eventDispatcher)
        {
            if(Player.TotalTime < Player.timerStartMilliseconds/2)
            {
                this.price = 200;
            }
            else if (Player.TotalTime >= Player.timerStartMilliseconds / 2)
            {
                this.price = 500;
            }
            this.Collision.AddPrimitive(new Box(this.Transform.Translation, Matrix.Identity, /*important do not change - cm to inch*/90f * this.Transform.Scale), new MaterialProperties());
        }
        
        public new object Clone(EventDispatcher eventDispatcher)
        {
            return new House("clone - " + ID, //deep
                this.ActorType,   //deep
                (Transform3D)this.Transform.Clone(),  //deep
                this.EffectParameters.GetDeepCopy(), //hybrid - shallow (texture and effect) and deep (all other fields) 
                this.Model,
                this.owner,
                eventDispatcher); //shallow i.e. a reference
        }

        public override void AddStatusFrom(Building newlyBuilt)
        {
            if (newlyBuilt.ID == "house")
            {
                // apply house effects
                //System.Diagnostics.Debug.WriteLine("theres a house near me");
            }
            else if (newlyBuilt.ID == "shopping center")
            {
                // apply effects to shopping center
                ShoppingCenter.totalIncome += 20;
            }
            else if (newlyBuilt.ID == "hotel")
            {
                // apply restaurant effects
                //Hotel.totalIncome -= 50;
            }
            // add more checks for more different building types
        }

        public override void RemoveStatusOf(Building building)
        {
            if (building.ID == "house")
            {
                //System.Diagnostics.Debug.WriteLine("A house was brought down nearby this house");
            }
            if (building.ID == "restaurant")
            {
                // apply restaurant effects
            }
            // add more checks for more different building types
        }

        public override bool Build()
        {

            if (this.IsValidPlaceToBuild && (this.owner.balance - this.price) >= 0)
            {
                if (Player.TotalTime >= Player.timerStartMilliseconds / 2)
                {
                    numberOfResidents = 15;
                }
                this.owner.cityCapacity += numberOfResidents;
                //emitter.Position = CalculateLocation(angle, distance);
                this.emitter.Position = this.Transform.Translation;
                object[] additionalParameters = { "BuildHouse" , this.emitter};
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound3D, additionalParameters));
            }

            return base.Build();
        }
    }
}