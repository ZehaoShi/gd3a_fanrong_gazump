﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class ShoppingCenter : Building
    {
        static int income = 0;
        static int incomeRate = 0;
        static int quantity = 0;
        public static int totalIncome = 0;
        public static int num = 0;


        public ShoppingCenter(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters, Model model, Player owner, EventDispatcher eventDispatcher) : base(id, actorType, transform, effectParameters, model, owner, eventDispatcher)
        {
            if (Player.TotalTime < Player.timerStartMilliseconds / 2)
            {
                this.price = 400;
            }
            else if (Player.TotalTime >= Player.timerStartMilliseconds / 2)
            {
                this.price = 800;
            }
            this.Collision.AddPrimitive(new Box(new Vector3(100, 300, this.Transform.Translation.Z), Matrix.Identity, /*important do not change - cm to inch*/50f * Vector3.One), new MaterialProperties());
        }

        public new object Clone(EventDispatcher eventDispatcher)
        {
            return new House("clone - " + ID, //deep
                this.ActorType,   //deep
                (Transform3D)this.Transform.Clone(),  //deep
                this.EffectParameters.GetDeepCopy(), //hybrid - shallow (texture and effect) and deep (all other fields) 
                this.Model,
                this.owner,
                eventDispatcher); //shallow i.e. a reference
        }

        public static int TotalIncome
        {
            get
            {
                return totalIncome;
            }
        }

        public static int Num
        {
            get
            {
                return num;
            }
        }

        public override bool Build()
        {
            if (this.IsValidPlaceToBuild && (this.owner.balance - this.price) >= 0)
            {
                // sound
                emitter.Position = this.Transform.Translation;
                object[] additionalParameters = { "BuildRestaurant", emitter };
                EventDispatcher.Publish(new EventData(EventActionType.OnPlay, EventCategoryType.Sound3D, additionalParameters));

                if (Player.TotalTime < Player.timerStartMilliseconds / 2)
                {
                    ShoppingCenter.totalIncome += 200;
                    this.owner.populationIncreaseRate += 5;
                }
                else if (Player.TotalTime >= Player.timerStartMilliseconds / 2)
                {
                    ShoppingCenter.totalIncome += 400;
                    this.owner.populationIncreaseRate += 5;
                }
            }

            return base.Build();
        }

        public override void AddStatusFrom(Building newlyBuilt)
        {
            if (newlyBuilt.ID == "house")
            {
                ShoppingCenter.totalIncome += 150;
            }
            else if (newlyBuilt.ID == "shopping center")
            {
                
            }
            else if (newlyBuilt.ID == "hotel")
            {
                // apply restaurant effects
                ShoppingCenter.totalIncome += 200;
            }
            // add more checks for more different building types
        }
    }
}
