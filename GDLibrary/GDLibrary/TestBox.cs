﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JigLibX.Collision;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class TestBox : CollidableObject
    {
        public TestBox(string id, ActorType actorType, Transform3D transform, EffectParameters effectParameters, Model model) : base(id, actorType, transform, effectParameters, model)
        {
            this.Collision.callbackFn += OnCollisionEvent;
        }

        public bool OnCollisionEvent(CollisionSkin collider, CollisionSkin collidee)
        {
            System.Diagnostics.Debug.WriteLine("Collision!!!"); 
            return false;
        }
    }
}
