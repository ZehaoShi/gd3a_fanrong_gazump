# README #

Getting the 3DGD code-base up and running

### What is this repository for? ###

* Support 3D Game Development module - DkIT
* 1.0

### How do I get set up? ###

* Requires Visual Studio 2017 and XNA Game Studio 4.0
* 3DS Max 2016 or later is useful for generating FBX models and animations
* An image editor (e.g. Gimp) is useful for producing textures with a transparent layer. Textures containing transparent pixels must be in PNG format.

### Who do I talk to? ###

* Niall McGuinness